function getHostName(){
	return window.location.origin.concat( window.location.pathname );
}

function findElementInTree(parent, elementID){
    if(parent == null)
        return null;

    if(parent.constructor === Array){
        var size = parent.length;
        var i = 0;
        for(i = 0; i< size; i++){
            if(parent[i].id == elementID){
                return parent[i];
            }else{
                if(parent[i].children != null){
                    var element = findElementInTree(parent[i].children, elementID);
                    if(element != null)
                        return element;
                }
            }
        }

    }else{
        if(parent.id == elementID)
            return parent;
        else
            return findElementInTree(parent.children, elementID);
    }
}

function getCurrentDate(){
    var currentDate = new Date();
    return currentDate.getFullYear() + "-" +
                                (currentDate.getMonth() < 9? "0" + (currentDate.getMonth() + 1): currentDate.getMonth() + 1) + "-" +
                                (currentDate.getDate() < 10? "0" + currentDate.getDate(): currentDate.getDate());
}

function getCurrentTime(){
    var currentDate = new Date();
    return getCurrentDate() + " " + currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
}

function clickCategoryIndexEvent(root){
    $(root + ' .glyphicon').click(function(){
        if(this.classList.contains("glyphicon-folder-close")){
            $(this).removeClass("glyphicon-folder-close");
            $(this).addClass("glyphicon-folder-open");
            $(this).parent().next().css("display","block");
        }else
            if(this.classList.contains("glyphicon-folder-open")){
                $(this).removeClass("glyphicon-folder-open");
                $(this).addClass("glyphicon-folder-close");
                $(this).parent().next().css("display","none");
            }
    });
}

function generateCodeCategoryIndex(category){
    var size = category.length;
    var i = 0;
    for(i = 0; i < size; i++){
        if(category[i].type == 'CATEGORY'){
            document.write('<li role="presentation" ng-click="selectCode($event)"><span class="glyphicon glyphicon-folder-close"> <a href="#" value="' + category[i].id + '"  role="tab" data-toggle="tab">' + category[i].name + '</a></li>');
            document.write('<div class="children" style="display:none; margin-left:0.5cm">');
                generateCategoryIndex(category[i].children);
            document.write('</div>');
        }else
            document.write('<li role="presentation" ng-click="selectCode($event)"><span class="glyphicon glyphicon-file"> <a href="#" value="' + category[i].id + '"  role="tab" data-toggle="tab">' + category[i].name + '</a></li>');
    }

}

function generateCategoryIndex(category){

    if(category.constructor === Array){
        var size = category.length;
        var i = 0;
        for(i = 0; i < size; i++){
            if(category[i].type == 'CATEGORY'){
                document.write('<li role="presentation" ng-click="selectEquipment($event)"><span class="glyphicon glyphicon-folder-close"> <a href="#" value="' + category[i].id + '"  role="tab" data-toggle="tab">' + category[i].name + '</a></li>');
                document.write('<div class="children" style="display:none; margin-left:0.5cm">');
                    generateCategoryIndex(category[i].children);
                document.write('</div>');
            }else
                document.write('<li role="presentation" ng-click="selectEquipment($event)"><span class="glyphicon glyphicon-file"> <a href="#" value="' + category[i].id + '"  role="tab" data-toggle="tab">' + category[i].name + '</a></li>');
        }
    }else{
        document.write('<li role="presentation" ng-click="selectEquipment($event)"><span class="glyphicon glyphicon-folder-close"> <a href="#" value="' + category.id + '"  role="tab" data-toggle="tab">' + category.name + '</a></li>');
        document.write('<div class="children" style="display:none; margin-left:0.5cm">');
            generateCategoryIndex(category.children);
        document.write('</div>');
    }
}

function convertNormalDate( date ){
    var $date = date.split( "-" );
    if( date.length >= 3 )
        return $date[2] + "-" + $date[1] + "-" + $date[0];
    else
        return date;
}

function convertNormalTime ( datetime ){
    var $datetime = datetime.split( " " );
    if( $datetime.length >= 2 )
        return convertNormalDate( $datetime[0] ) + " " + $datetime[1];
    else
        return datetime;
}

function convertNormalDuration( $duration ){
    var $convert = 1;
    for( var i = settings.system_time_format.value - 1; i < settings.system_time_format.range.length; i++ ){
        $convert *= settings.system_time_format.range[i].value;
    }
    return ( $duration / $convert ) + " " + settings.system_time_format.range[settings.system_time_format.value - 1].symbol
}