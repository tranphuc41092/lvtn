<?php
	session_set_cookie_params(0);
	session_start();
	error_reporting(E_ALL ^ E_DEPRECATED);
	include "core.php";
	$__profile = null;
	$__post_result = null;

	includeAllPhp("utils");
	includeAllPhp("model");
	includeAllPhp("process");

	$__settings = new Settings();
	$__settings = convert_assoc($__settings->find(), "variable");
	$__settings["system_time_format"]->range = $__settings["system_time_format"]->getChildren();

?>


<head>
    <link rel="icon" href="images/favicon.gif" type="image/gif">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Software Project Management</title>

	<script type="text/javascript" src="<?=SITE ?>script/jquery.js"></script>
	<script type="text/javascript" src="<?=SITE ?>script/script.js"></script>
	<link rel="stylesheet" href="<?=SITE ?>bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=SITE ?>bootstrap/css/bootstrap-theme.min.css">
	<script src="<?=SITE ?>bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=SITE ?>bootstrap/js/angular.min.js"></script>
    <link rel="stylesheet" href="<?=SITE ?>bootstrap/css/style.css" type="text/css" media="screen" />
    <script type="text/javascript" src="<?=SITE ?>angualarjs/jquery-1.11.1.min.js"></script>

    <link rel="stylesheet" href="<?=SITE ?>bootstrap/css/bootstrap-multiselect.css" type="text/css">
    <script type="text/javascript" src="<?=SITE ?>bootstrap/js/bootstrap-multiselect.js"></script>
    <script>
        var settings = <?=json_encode($__settings, JSON_UNESCAPED_UNICODE)?>;
        var profile = <?=json_encode($__profile, JSON_UNESCAPED_UNICODE)?>;
		settings.convertNormalDate = function ( date ){
		    var $date = date.replace( / .*/g, "").split( "-" );
		    var month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		    if( date.length >= 3 )
		        return $date[2] + "-" + month_names_short[parseInt($date[1]) - 1] + "-" + $date[0];
		    else
		        return date;
		}

		settings.convertNormalDateTime = function ( datetime ){
		    var $datetime = datetime.split( " " );
		    if( $datetime.length >= 2 )
		        return convertNormalDate( $datetime[0] ) + " " + $datetime[1];
		    else
		        return datetime;
		}

		settings.convertNormalDuration = function( $duration, $symbol ){
		    var $convert = 1;
		    for( var i = this.system_time_format.value - 1; i < this.system_time_format.range.length; i++ ){
		        $convert *= this.system_time_format.range[i].value;
		    }
		    return ($duration / $convert) +  ( $symbol != true? "": " " + this.getSystemTimeFormatSymbol() + ($duration / $convert > 1? '(s)': ''));
		}

		settings.getSystemTimeFormatSymbol = function(){
			return this.system_time_format.range[this.system_time_format.value - 1].symbol;
		}

		settings.showProgress = function( duration1, duration2 ){
			if( duration1 >= duration2 ){
				return 100;
			}else{
				return duration1 * 100/ duration2;
			}
		}
		settings.compareTime = function(time1, time2){
        	return new Date(time1) > new Date(time2);
        }

		settings.currentDate = getCurrentDate();
		settings.currentTime = getCurrentTime();
		settings.hostname = window.location.origin.concat( window.location.pathname );
		settings.currentURL = window.location.href;
		profile.isAdmin = function(){
			return ( this.position == 'ADMIN' );
		}

		profile.isLeader = function(){
			return ( this.position == 'LEADER' );
		}

		profile.isEngineer = function(){
			return ( this.position == 'STAFF' );
		}
	</script>
</head>

<body>
	<div class="container">
		<div class="panel panel-default">
			<div id="menu-bar">
				<?php include "page-template/navigation.php"; ?>
			</div>
			<!-- Body -->
			<div id="body" style="min-height: 505px; margin-bottom: 20px">

				<?php include "page-template/body.php"; ?>

			</div>

			<!-- footer -->
			<div  id="footer" style="background-color: #E73819; ">
				<?php include "page-template/footer.php"; ?>
			</div>

		</div>
	</div>
</body>
<?php ob_end_flush(); ?>