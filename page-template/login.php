<?php
	if(!$__profile){
?>
		<div class="panel panel-default">
			<fieldset  style="width: 90%; margin: 0 auto;">
				   <form class="form-signin" role="form" method="post">
					<h2 class="form-signin-heading">Log In</h2>
					<div class="error" role="alert" style="color:red;"><?=$__post_result? 'User name or password was wrong!': ''?></div>

					<label for="username">Username</label>
					<input type="text" id="username" name="username" class="form-control" placeholder="User name" required="" autofocus="">
					<label for="password">Password</label>
					<input type="password" id="password" name="password" class="form-control" placeholder="Password" required="">
					<div class="checkbox">
					  <label>
						<input type="checkbox" name="remember" value="remember">Remember
					  </label>
					</div>
					<div><a href="?cat=staff&action=register">Sign up new account</a></div>
					<!-- <div><a href="#">Forget password?</a></div> -->
					<br />
					<input type="hidden" name="action" value="login"/>
					<button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button><br>
				  </form>

			</fieldset>
		</div>
<?php
	}
?>