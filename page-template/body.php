<?php
	if($__profile){
		if(!isset($_GET[CAT])){
			include "function/home/index.php";
		}else{
			$filename = "";
			$core = "";

			if(isset($_GET[ACTION])){
				$filename = "function/".$_GET[CAT]."/".$_GET[ACTION].".php";
			}else{
				$filename = "function/".$_GET[CAT]."/index.php";
			}

			if(file_exists($filename)){
				include $filename;
			}else{
				echo "Page not exist!<br>
					<a href='/'>Come back home</a>";
			}
		}

	}else{
		if(isset($_GET["cat"]) && isset($_GET["action"]) && $_GET["cat"] == "staff" && $_GET["action"] == "register"){
			include "function/staff/register.php";
		}else{
			echo '
				<div class="home row">
					<div class="left col-md-3">';

			include "page-template/login.php";

			echo '
					</div>
					<div class="right col-md-9">
						<div class="panel">';
			include "function/home/index.php";
			echo '
						</div>
					</div>
				</div>
			</div>
			';
		}

	}


?>