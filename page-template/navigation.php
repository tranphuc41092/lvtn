	<nav class="navbar navbar-default" role="navigation">
	  <div class="container-fluid">

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav">
			<li class=""><a href="/tttn">Home</a></li>
<?php
		if($__profile){
?>
			<li class=""><a href="?cat=project">Project</a></li>
			<li class=""><a href="?cat=staff">Human</a></li>
			<li class=""><a href="?cat=equipment">Equipment</a></li>
			<li class=""><a href="?cat=policy">Policy</a></li>
<?php
		}
?>
		  </ul>
<?php
		if($__profile){
?>
		  <div class="nav navbar-nav navbar-right">
			<div class="navbar-form navbar-right" style="float:right">
				<form  role="form" method="post" style="margin: auto 0px" action="<?php echo SITE;?>">
					<a href="?cat=staff&action=profile">
						<?php echo  $__profile->name;?>
						<image src="<?php if($__profile->avatar=='') echo 'images/avatar/profile/profile_image.jpg'; else echo  $__profile->avatar;?>" style="height: 35px; border-radius: 5px; "/>
					</a>
					<input type="hidden" name="action" value="logout" />
					<button type="submit" class="btn btn-default navbar-right">Log out</button>
				</form>
			</div>
		  </div>
<?php
		}
?>

		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
