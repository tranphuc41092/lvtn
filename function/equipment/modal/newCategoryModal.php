<!--New Category Modal -->
<div class="modal fade" id="newCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Category</h4>
            </div>
            <form class="form-horizontal" method="post" ng-submit="submit()" action="{{currentURL}}" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" placeholder="Enter name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="avatar" class="col-sm-3 control-label">Image</label>
                        <div class="col-sm-9">
                            <image src="images/avatar/equipment/equipment.png" style="width: 100px;"/>
                            <input type="file" class="form-control" name="avatar" accept="image/*" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea type="text" name="description" class="form-control" rows="5" placeholder="Enter description"></textarea>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" name="action" value="newEquipment">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
