<!--edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit {{equipment.type}}</h4>
            </div>
            <form class="form-horizontal" method="post" ng-submit="submit()" action="{{currentURL}}"  enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" ng-model="equipment.name" placeholder="Enter name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="avatar" class="col-sm-3 control-label">Image</label>
                        <div class="col-sm-9">
                            <image src="{{equipment.avatar==''? 'images/avatar/equipment/equipment.png': equipment.avatar}}" style="width: 100px; "/>
                            <input type="file" class="form-control" name="avatar" accept="image/*" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea type="text" name="description" ng-model="equipment.description" class="form-control" rows="5" placeholder="Enter description"></textarea>
                        </div>
                    </div>

                    <input type="hidden" name="action" value="editEquipment" />
                    <input ng-hide="true" id="id" name="id"  ng-model="equipment.id" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
