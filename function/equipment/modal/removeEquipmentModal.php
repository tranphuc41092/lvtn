    <!--Remove Modal -->
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header alert alert-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Remove {{equipment.type}}</h4>
                </div>
                
                <form class="form-horizontal" method="post" action="{{currentURL}}">
                    <div class="modal-body">
                        <div>
                            <h4>Do you really want to remove <br>{{equipment.name}} <image src="{{equipment.avatar==''? 'images/avatar/equipment/equipment_image.jpg': equipment.avatar}}" style="width: 150px"/> ?</h4>                            
                            <br>
                            <input type="hidden" name="action" value="removeEquipment" />
                            <input ng-hide="true" id="id" name="id" ng-model="equipment.id" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>                    
                </form>                
            </div>
        </div>
    </div>
