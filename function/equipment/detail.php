<div ng-app="myApp" ng-controller="equipmentController">
    <h2><span class="label label-danger">Equipment Management</span></h2>
    <br>

<?php includeAllPhp("function/equipment/modal"); ?>

<?php
	if(isset($_GET["id"])){
		$equipment = new Equipment($_GET["id"]);

		if($equipment->id){
			$equipment->getStructureChildren();
			$equipment->path = $equipment->getPath();
            echo '
                <script>
                    var equipment = '.json_encode($equipment, JSON_UNESCAPED_UNICODE).';
                </script>
            ';
?>
		<script type="text/javascript">
		    var app = angular.module('myApp', []);
		    app.controller('equipmentController', function($scope) {
		        $scope.currentURL = document.URL;
		        $scope.equipment = equipment;
		    });
		</script>

	    <a href="?cat=equipment">Category</a>
	    <span ng-repeat="path in equipment.path"> ---> <a href="?cat=equipment&action=detail&id={{path.id}}">{{path.name}}</a></span>
	    <div role="tabpanel">

        <button class="btn btn-default" data-toggle="modal" data-target="#editModal" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true">Edit</button>

		<table class="table">
			<tr>
				<th>Name</th>
				<td>{{equipment.name}}</td>
			</tr>
            <tr>
                <th>Image</th>
                <td><image src="{{equipment.avatar==''? 'images/avatar/equipment/equipment_image.jpg': equipment.avatar}}" style="width: 150px"/></td>
            </tr>
			<tr>
				<th>Description</th>
				<td>{{equipment.description}}</td>
			</tr>
			<tr>
				<th>Type</th>
				<td>{{equipment.type}}</td>
			</tr>
            <tr ng-if="equipment.type=='CATEGORY'">
                <th>Subcategory</th>
                <td>
                    <div ng-repeat="child in equipment.children">
                        <div ng-if="child.type=='CATEGORY'">
                            <a href="?cat=equipment&action=detail&id={{child.id}}">{{child.name}}({{child.children.length}})</a>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <th>List of equipment</th>
                <td>
                	<table class="table">
                        <tr ng-repeat="child in equipment.children" ng-if="child.type!='CATEGORY'">
                                <td><a href="?cat=equipment&action=detail&id={{child.id}}">{{child.name}}</a><td>
                                <td>{{child.description}}
                        </tr>
                	</table>
                </td>
            </tr>

            <tr ng-if="equipment.type!='CATEGORY'">
            	<th>History</th>
                <td>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Task</th>
                                <th>Start time</th>
                                <th>End time</th>
                            </tr>
                        </thead>
                        <tr ng-repeat="assign in equipment.assign">
                            <th>{{assign.task.name}}</th>
                            <td>{{assign.starttime}}</td>
                            <td>{{assign.endtime}}</td>
                        </tr>
                    </table>
                </td>

            </tr>
		</table>

<?php
		}else{
			echo "There no equipment!";
		}
	}else{
		echo "There no equipment!";
	}
?>

</div>

