<?php
    $equipment = new Equipment();
    $equipments = $equipment->find(array("belongto"=>0));
    if(isset($_GET["id"])){
        $equipments = $equipment->find(array("id"=>$_GET["id"]));
    }
    echo '
        <script>
            var equipments = '.json_encode($equipments, JSON_UNESCAPED_UNICODE).';
        </script>';
?>

<script type="text/javascript">
    var app = angular.module('myApp', []);
    app.controller('equipmentController', function($scope) {
        $scope.myMain = true;
        $scope.currentURL = document.URL;
        $scope.equipments = equipments;
        $scope.settings = settings;
        $scope.equipment = equipments[0];
        $scope.profile = profile;
        $scope.selectEquipment = function(event){
            var id = event.target.attributes[1].value
            $scope.equipment = findElementInTree(equipments, id);
        }
    });

</script>
<style type="text/css">
.scroll-area {
    height: 430px;
    position: relative;
    overflow: auto;
}
</style>
<div ng-app="myApp" ng-controller="equipmentController">
    <h2><span class="label label-success">Equipment Management</span></h2>
    <br>
    <div class="row" style="width:95%; margin:auto;">
        <div class="col-md-3 navigation">
            <button class="btn btn-default" data-toggle="modal" data-target="#newCategoryModal" ng-if="profile.isAdmin()" title="New Category"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>New Category</button>
            <div class="scroll-area" role="tabpanel">
            <font size="3">
                <script type="text/javascript">
                    document.write('<ul id="categoryIndex" class="nav nav-stacked" role="tablist">')
                        generateCategoryIndex(equipments);
                    document.write("</ul>")
                    clickCategoryIndexEvent('#categoryIndex');
                </script>
            </font>
            </div>

        </div>
        <div class="col-md-9 main">
            <div ng-show="equipment == null">
                <button class="btn btn-default" data-toggle="modal" data-target="#newModal" title="New"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
            </div>
            <div ng-hide="equipment == null">
            	<table ng-if="profile.isAdmin()">
            		<tr>
            			<td>
			                <button ng-if="equipment.type=='CATEGORY'" class="btn btn-default" data-toggle="modal" data-target="#newModal" title="New"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>New</button>
			                <button class="btn btn-default" data-toggle="modal" data-target="#editModal" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>Edit</button>
			                <button class="btn btn-default" data-toggle="modal" data-target="#removeModal" title="Remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Remove</button>
            			</td>
            		</tr>
            	</table>

                <div class="scroll-area-left">
					<table class="table table-striped table-bordered">
	                    <tr>
	                        <th style="width: 30%;">Name</th>
	                        <td>{{equipment.name}}</td>
	                    </tr>
	                    <tr>
	                        <th>Image</th>
	                        <td><image src="{{equipment.avatar==''? 'images/avatar/equipment/equipment_image.jpg': equipment.avatar}}" style="width: 150px"/></td>
	                    </tr>
						<tr>
							<th>Description</th>
							<td>{{equipment.description}}</td>
						</tr>
	<!--                     <tr ng-if="equipment.type=='CATEGORY'">
	                        <th>Subcategory</th>
	                        <td>
	                            <div ng-repeat="child in equipment.children">
	                                <div ng-if="child.type=='CATEGORY'">
	                                    <a href="?cat=equipment&action=detail&id={{child.id}}">{{child.name}}({{child.children.length}})</a>
	                                </div>
	                            </div>
	                        </td>
	                    </tr>

	                    <tr ng-if="equipment.type=='CATEGORY'">
	                        <th>List of equipment</th>
	                        <td>
	                            <div ng-repeat="child in equipment.children">
	                                <div ng-if="child.type!='CATEGORY'">
	                                    <a href="?cat=equipment&action=detail&id={{child.id}}">{{child.name}}</a>
	                                </div>
	                            </div>
	                        </td>
	                    </tr>
	 -->
	                    <tr ng-if="equipment.type!='CATEGORY'">
	                        <th>History</th>
	                        <td>
	                            <table class="table">
	                                <thead>
	                                    <tr>
	                                        <th>Project</th>
	                                        <th>Task</th>
	                                        <th>Start time</th>
	                                        <th>End time</th>
	                                    </tr>
	                                </thead>
	                                <tr ng-repeat="assign in equipment.assign">
	                                    <th>{{assign.task.project.name}}</th>
	                                    <th><a href="?cat=task&id={{assign.task.id}}">{{assign.task.name}}</a></th>
	                                    <td>{{settings.convertNormalDateTime(assign.starttime)}}</td>
	                                    <td>{{settings.convertNormalDateTime(assign.endtime)}}</td>
	                                </tr>
	                            </table>
	                        </td>
	                    </tr>

					</table>
                </div>
			</div>
        </div>
    </div>

    <?php
        includeAllPhp("function/equipment/modal");
    ?>
</div>
