<?php
	if($__profile){
?>		
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
 		<!-- Indicators -->
  		<ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	  	</ol>

	  	<!-- Wrapper for slides -->
	  	<div class="carousel-inner" role="listbox" >
	    	<div class="item active">
	      		<img src="images/home/home5.jpg" alt="..." style="height:500px; width:100%; ">
		      	<div class="carousel-caption" style="margin-left:-90px; text-align:left; margin-bottom:350px;">		        	    	
		        	<a href="?cat=project" class="alert-link"><h1>Project Management</h1></a>	
		      	</div>

	    	</div>
	    	<div class="item">
	      		<img src="images/home/home6.jpg" alt="..." style="height:500px; width:100%;">
	      		<div class="carousel-caption" style="text-align:right; margin-right:-90px;">
	      			<a href="?cat=staff" class="alert-link"><h1>Project Human</h1></a>
	      			
	      		</div>
	    	</div>
	    	<div class="item">
	      		<img src="images/home/home7.jpg" alt="..." style="height:500px; width:100%;">
	      		<div class="carousel-caption" style="margin-left:-90px; text-align:left; margin-bottom:250px;">
	      			<a href="?cat=equipment" class="alert-link"><h1>Project Equipment</h1></a>
	      			
	      		</div>
	    	</div>
	  	</div>

	  	<!-- Controls -->
	  	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    	<span class="sr-only">Previous</span>
	  	</a>
	  	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    	<span class="sr-only">Next</span>
	  	</a>
	</div>
<?php		
	}else{
?>
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
 		<!-- Indicators -->
  		<ol class="carousel-indicators">
		    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
		    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	  	</ol>

	  	<!-- Wrapper for slides -->
	  	<div class="carousel-inner" role="listbox" >
	    	<div class="item active">
	      		<img src="images/home/home1.jpg" alt="..." style="height:75%; width:100%;">
		      	<div class="carousel-caption">
		        	<h1>Welcome To Website</h1>    				
		      	</div>
	    	</div>
	    	<div class="item">
	      		<img src="images/home/home2.jpg" alt="..." style="height:75%; width:100%;">
	      		<div class="carousel-caption">
	        		<h1>Software Development Project Management</h1>
	      		</div>
	    	</div>
	    	<div class="item">
	      		<img src="images/home/home3.jpg" alt="..." style="height:75%; width:100%;">
	      		<div class="carousel-caption">
	        		<h1>Dedicated To The Software Projects</h1>
	      		</div>
	    	</div>
	  	</div>

	  	<!-- Controls -->
	  	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    	<span class="sr-only">Previous</span>
	  	</a>
	  	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    	<span class="sr-only">Next</span>
	  	</a>
	</div>
<?php
	}
?>
