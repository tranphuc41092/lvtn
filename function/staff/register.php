<br><h2><span class="label label-danger">Create Account</span></h2><br><br>
<?php
//-----------------------//
if(!$__profile){
?>
	<div id="regis"class="centered-form" id="abc" style="width: 80%; margin: 0px auto;">
   		<form role="form" method="post" enctype="multipart/form-data">
			<div  >
				<div class="alert alert-danger" style="color: red">
					<span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>Information Required
						<?php
							if($__post_result)
								echo '<br><strong><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>'.$__post_result["text"].'</strong>';
						?>
					</div>

				</div>


				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="username">User name:</label>
						  	<input type="text" name="username" class="form-control" id="username" aria-describedby="inputSuccess2Status" placeholder="Enter your account" required value="<?= $__post_result? $__post_result['object']['username']: '' ?>">
						  	<span class="glyphicon glyphicon-asterisk form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="password">Password:</label>
						  	<input type="password" name="password" class="form-control" id="password" aria-describedby="inputSuccess2Status" placeholder="Enter your password" required>
						  	<span class="glyphicon glyphicon-asterisk form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="repassword">Re-Password:</label>
						  	<input type="password" name="repassword" class="form-control" id="repassword" aria-describedby="inputSuccess2Status" placeholder="Re-enter your password" required>
						  	<span class="glyphicon glyphicon-asterisk form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="name">Full Name:</label>
						  	<input type="text" name="name" class="form-control" id="name" aria-describedby="inputSuccess2Status" placeholder="Enter your full name" required value="<?= $__post_result? $__post_result['object']['name']: '' ?>">
						  	<span class="glyphicon glyphicon-asterisk form-control-feedback" aria-hidden="true"></span>
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="name" style="margin-right: 20px;">Sex:</label>
						  	<input name="sex" id="sex" type="radio" name="sex" value="0" checked>Male
						  	<input name="sex" id="sex" type="radio" name="sex" value="1" style="margin-left: 10px;">Female
						</div>
					</div>
  					<div class="col-md-6">
  						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="birthdate">Birthday: <font style="font-style:italic;">(day/month/year)</font></label>
						  	<input type="date" name="birthdate" id="birthdate" class="form-control" aria-describedby="inputSuccess2Status" value="<?= $__post_result? $__post_result['object']['birthdate']: '' ?>">
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="email">E-mail:</label>
						  	<input type="email" name="email" id="email" class="form-control" aria-describedby="inputSuccess2Status" placeholder="Enter your e-mail" value="<?= $__post_result? $__post_result['object']['email']: '' ?>">
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="telephone">Mobile number:</label>
						  	<input type="tel" name="telephone" id="telephone" class="form-control" aria-describedby="inputSuccess2Status" placeholder="Enter your mobile number" value="<?= $__post_result? $__post_result['object']['telephone']: '' ?>">
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="address">Address:</label>
						  	<input type="text" name="address" id="address" class="form-control" aria-describedby="inputSuccess2Status" placeholder="Enter  your address" value="<?= $__post_result? $__post_result['object']['address']: '' ?>">
						</div>
						<div class="form-group has-success has-feedback">
						  	<label class="control-label" for="skill">Skill:</label>
						  	<select name="skill[]" id="skill-getting-started" multiple="multiple">
							    <option value="java">JAVA</option>
							    <option value="c++">C++</option>
							    <option value="c#">C#</option>
							    <option value="php">PHP</option>
							    <option value="htlm">HTML</option>
							    <option value="otherskill" class="form-inline form-group">Other</option>
							</select>
						</div>
					</div>
  				</div>
				<input type="hidden" name="action" value="register"/>
			</div>
			<input type="submit" value="Sign Up" class="btn btn-info btn-block" style="width:20%; margin:0 auto; margin-bottom: 20px;">
		</form>
    </div>

<?php
}else{
//	alert("You was logged in! \\n Come back home!");
	home();
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#skill-getting-started').multiselect({
        	includeSelectAllOption: true,
        	enableFiltering: true,
        	buttonWidth: '100%',
        	maxHeight: 150
        });
    });
</script>