<h2><span class="label label-success">Profile</span></h2>
<br>
<br>

<?php
$profile = authenticate();
$canChange = true;
if(isset($_GET["id"])){
	$profiles = $profile->find(array("id"=>$_GET["id"]));
	if(sizeof($profiles) > 0)
		$profile = $profiles[0];
	else
		$profile = null;
	$canChange = false;
}


if($profile){
	$profile->experience = $profile->getExperience();
echo '
	<script>
		var profile = '.json_encode($profile, JSON_UNESCAPED_UNICODE).';
	</script>';
?>
<script type="text/javascript">
    var app = angular.module('myApp', []);
    app.controller('staffController', function($scope) {
        $scope.currentURL = document.URL;
        $scope.staff = profile;
    });
   	app.filter('html', function ($sce) {
	    return function (text) {
	        return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br/>')) : '';
	    };
	});

</script>
<div ng-app="myApp" ng-controller="staffController">
    <div style="width:80%; margin:0px auto;">
<?php
		if($canChange){
			echo '<button class="btn btn-default" data-toggle="modal" data-target="#editModal">Change</button><br /><br />';
			include "function/staff/modal/editProfileModal.php";
		}
?>

		<table class="table table-bordered">
			<tr>
				<th>Name</th>
				<td>{{staff.name}}</td>
            <tr>
                <th>Avatar</th>
                <td><image src="{{staff.avatar==''? 'images/avatar/profile/profile_image.jpg': staff.avatar}}" style="width: 100px; height: 100px;"/></td>
            </tr>

            <tr>
                <th>Sex</th>
                <td ng-if="staff.sex==0">Male</td>
                <td ng-if="staff.sex==1">Female</td>
            </tr>
            <tr>
                <th>Birthdate</th>
                <td>{{staff.birthdate}}</td>
            </tr>
            <tr>
                <th>Address</th>
                <td>{{staff.address}}</td>
            </tr>
            <tr>
                <th>Phone</th>
                <td>{{staff.telephone}}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{staff.email}}</td>
            </tr>
            <tr>
                <th>Position</th>
                <td>{{staff.position}}</td>
            </tr>
			<tr>
				<th>Description</th>
				<td>{{staff.description}}</td>
			</tr>
		</table>

		<div ng-if="staff.experience.length != 0">
			<h4>He/she took park in projects</h4>
			<table class="table table-bordered">
				<tr>
					<th>Project</th>
					<th>Comment</th>
					<th>Rate</th>
				</tr>
				<tr ng-repeat="experience in staff.experience">
					<td><a href="?cat=task&id={{experience.project.id}}" >{{experience.project.name}}</a></td>
					<td ng-bind-html="experience.comment | html"></td>
					<td>{{experience.rate}}</td>
				</tr>
			</table>
		</div>

    </div>

</div>

<?php
}
?>