<h2><span class="label label-success">Staff Management</span></h2>
<br>
<br>

<?php
    $team = new Team();
    $project = new Task();

    $teams = $team->find();
    $projects = $project->find(array("type"=>"PROJECT"));
    foreach ($projects as $project) {
        $project->assignee = $project->getAssignee();
        $project->leader = $project->getLeader();
    }

    echo '
        <script>
            var teams = '.json_encode($teams, JSON_UNESCAPED_UNICODE).';
            var projects = '.json_encode($projects, JSON_UNESCAPED_UNICODE).';
        </script>
    ';

?>
<style type="text/css">
    table tr td button{
        min-width: 70px;
    }
    .member-area{
        overflow: auto;
        max-height: 350px;
    }
    .member-area1{
        overflow: auto;
        max-height: 200px;
    }
</style>

<script type="text/javascript">
    var app = angular.module('myApp', []);
    app.controller('staffController', function($scope) {
        $scope.currentURL = document.URL;
        $scope.staff = {"id": "0"};
        $scope.teams = teams;
        $scope.team = teams[0];
        $scope.projects = projects;
        $scope.settings = settings;
        $scope.profile = profile;
        $scope.chooseTeam = function(e){
            var id = $( e.target ).attr( 'value' );
            $scope.team = findElementInTree(teams, id);
        }
        $scope.selectMember = function( staff ){
            $scope.staff = staff;
        }
    });
</script>

<div ng-app="myApp" ng-init="myMain=true" ng-controller="staffController" style="width:95%; margin:0 auto;">
    <div role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#company" aria-controls="company" role="tab" data-toggle="tab">Human company structure</a></li>
            <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Human project structure</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" >
            <?php includeAllPhp("function/staff/tab")?>
        </div>

    </div>

    <?php includeAllPhp("function/staff/modal")?>
</div>
