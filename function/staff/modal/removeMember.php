<!--Remove Modal -->
<div class="modal fade" id="removeMember" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Remove Member</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
                    <input type="hidden" name="id" value="{{member.id}}" />
                    <div style="text-align: center">
                        <h4>Do you want to remove this member?</h4>
                        <input type="hidden" name="action" value="removeMember" />
                        <input ng-hide="true" id="id" name="id" ng-model="staff.id" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>

