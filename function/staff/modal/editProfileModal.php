        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myEditModalLabel" aria-hidden="true">
            <div class="modal-dialog"   style="width: 700px;">
                <div class="modal-content">
                    <div class="modal-header alert alert-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Profile</h4>
                    </div>
                    <form method="post" ng-submit="submit()" action="{{currentURL}}" enctype="multipart/form-data">
	                    <div class="modal-body">
                        	<table class="table table-striped table-bordered" style="width: 90%; margin: 0 auto;">
	                        	<tr>
	                        		<th>Name:</th>
	                        		<td><input type="text" name="name" class="form-control" ng-model="staff.name" placeholder="Enter name" required></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Avatar</th>
	                        		<td>
	                        			<image src="{{staff.avatar==''? 'images/avatar/profile/profile_image.jpg': staff.avatar}}" style="width: 100px; height: 100px;"/>
	                        			<input type="file" class="form-control" name="avatar" accept="image/*" />
	                        		</td>
	                        	</tr>
	                        	<tr>
	                        		<th>Password:</th>
	                        		<td><input type="password" name="password" class="form-control" ng-model="staff.password" required></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Re-Password:</th>
	                        		<td><input type="password" name="repassword" class="form-control" ng-model="staff.repassword" required></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Sex:</th>
	                        		<td>
	                        			<select name="sex" class="form-control">
	                        				<option value="0" ng-selected="staff.sex==0">Male</option>
	                        				<option value="1" ng-selected="staff.sex==1">Female</option>
	                        			</select>
	                        		</td>
	                        	</tr>
	                        	<tr>
	                        		<th>Birthdate:</th>
	                        		<td><input type="date" name="birthdate" class="form-control" ng-model="staff.birthdate" placeholder="Enter birth date"></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Address:</th>
	                        		<td><input type="text" name="address" class="form-control" ng-model="staff.address" placeholder="Enter address"></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Phone:</th>
	                        		<td><input type="text" name="telephone" class="form-control" ng-model="staff.telephone" placeholder="Enter phone"></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Email:</th>
	                        		<td><input type="text" name="email" class="form-control" ng-model="staff.email" placeholder="Enter email"></td>
	                        	</tr>
	                        	<tr>
	                        		<th>Description:</th>
	                        		<td> <textarea type="text" name="description" ng-model="staff.description" class="form-control" rows="5" placeholder="Enter description"></textarea></td>
	                        	</tr>
	                        	<input type="hidden" name="action" value="editProfile" />
                                <input ng-hide="true" id="id" name="id"  ng-model="staff.id" />
                        	</table>
                    	</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">OK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
