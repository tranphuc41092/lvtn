        <!--edit Modal -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myEditModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header alert alert-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit Staff</h4>
                    </div>
                    
                    <form class="form-horizontal" method="post" ng-submit="submit()" action="{{currentURL}}" >
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" ng-model="staff.name" placeholder="Enter name" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="sex" class="col-sm-3 control-label">Sex</label>
                                <div class="col-sm-9">
                                    <input type="text" name="sex" class="form-control" ng-model="staff.sex" placeholder="Enter sex">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="birthdate" class="col-sm-3 control-label">Birthdate</label>
                                <div class="col-sm-9">
                                    <input type="date" name="birthdate" class="form-control" ng-model="staff.birthdate" placeholder="Enter birth date">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    <input type="text" name="address" class="form-control" ng-model="staff.address" placeholder="Enter address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="telephone" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" name="telephone" class="form-control" ng-model="staff.telephone" placeholder="Enter phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    <input type="text" name="email" class="form-control" ng-model="staff.email" placeholder="Enter email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="position" class="col-sm-3 control-label">Position</label>
                                <div class="col-sm-9">
                                    <input type="text" name="position" class="form-control" ng-model="staff.position" placeholder="Enter position">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="comment" class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea type="text" name="description" ng-model="staff.description" class="form-control" rows="5" placeholder="Enter description"></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="action" value="editStaff" />
                            <input ng-hide="true" id="id" name="id"  ng-model="staff.id" />                            
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">OK</button>
                        </div>
                    </form>                    
                </div>
            </div>
        </div>

