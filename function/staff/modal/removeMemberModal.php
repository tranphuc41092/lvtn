<!--Remove Modal -->
<div class="modal fade" id="removeMember" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Remove member</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="{{currentURL}}">
                    <input type="hidden" name="id" value="{{member.id}}" />
                    <div>
                        <h3><span>Do you really want to remove member {{staff.name}} of {{team.name}}?</span></h3>
                        <input ng-hide="true" id="id" name="id" ng-model="staff.id" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit"  name="action" value="removeMember" class="btn btn-primary">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

