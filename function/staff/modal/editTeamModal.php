<!--Edit Team Modal -->
<div class="modal fade" id="editTeamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Team</h4>
            </div>

            <form class="form-horizontal" method="post" ng-submit="submit()" action="{{currentURL}}" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Team Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" value="{{team.name}}" placeholder="Enter Team's name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="avatar" class="col-sm-3 control-label">Image</label>
                        <div class="col-sm-9">
                            <image src="{{team.avatar==''? 'images/avatar/team/teamwork.png': team.avatar}}" style="width: 100px; "/>
                            <input type="file" class="form-control" name="avatar" accept="image/*" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="leaderid" class="col-sm-3 control-label">Leader</label>
                        <div class="col-sm-9">
                             <select name="leaderid" class="form-control">
                                <option value="{{team.leader.id}}" selected>{{team.leader.name}}</option>
                                <option ng-repeat="member in teams[0].members" value="{{member.id}}" ng-if="member.position == 'STAFF'">{{member.name}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea type="text" name="description" value="{{team.description}}" class="form-control" rows="5" placeholder="Enter description"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="members" class="col-sm-3 control-label">Member</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-md-6" ng-repeat="member in team.members">
                                    <input type="checkbox" name="members[]" value="{{member.id}}" checked />{{member.name}}
                                </div>
                                <div class="col-md-6" ng-repeat="member in teams[0].members">
                                    <input type="checkbox" name="members[]" value="{{member.id}}" />{{member.name}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{team.id}}" />
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" name="action" value="editTeam">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>

