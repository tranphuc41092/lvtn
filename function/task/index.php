<?php
    if(isset($_GET["id"])){
        $task = new Task($_GET["id"]);
        if($task->id != ""){
            $task->children = $task->getChildren();
            $task->parent = $task->getParent();
            $task->comment = $task->getComment();
            $task->assignee = $task->getAssignee();
            $task->leader = new Staff( $task->leaderid );

            $task->assigneed = $task->getAssigneed();
            $task->notassignee = $task->getNotAssignee();
            $task->report = $task->getReport();
            $task->project = $task->getProject();
            if( $task->status == 2 ){
               $task->finished = $task->getFinishedResult();
            }

            $task->timelog_of_task = $task->getTimelog_of_task();
            $task->timelog_of_assignee = $task->getTimelog_of_assignee( $__profile->id );

            $task->worklogs = $task->get_worklog();

            $task->path = $task->getPath();

            $task->code = new Code("code/code_".$task->getProject()->id);
            $task->release = new Code( 'code/release/code_'.$task->getProject()->id);

            $assign = new Assign();
            $task->equipment = $assign->find(array("type"=>"Equipment", "taskid"=>$task->id));
            $task->hasTemporyCode = $task->hasTemporyCode();

            $equipment = new Equipment();
            $equipments = $equipment->find(array("belongto"=>"0"));

            echo '
                <script>
                    var task = '.json_encode($task, JSON_UNESCAPED_UNICODE).';
                    var equipments = '.json_encode($equipments, JSON_UNESCAPED_UNICODE).';
                    task.isProject = function(){
                    	return task.type == "PROJECT";
                    }
                    task.isPublic = function(){
                    	return task.projecttype == "PUBLIC";
                    }
                </script>
            ';
?>
<script type="text/javascript">
    var app = angular.module('myApp', []);
	app.filter('html', function ($sce) {
	    return function (text) {
	        return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br/>')) : '';
	    };
	});
	for( var release in task.release.children ){
		task.release.children[release].name = task.release.children[release].name.replace( /\.zip.*/g, '');
	}
	task.code.name = task.project.name;
	task.release.name = task.project.name;

    app.controller('projectController', function($scope) {
        $scope.compareTime = function(time1, time2){
            return new Date(time1) > new Date(time2);
        }

        $scope.task = task;
        $scope.subtask = {startdate : settings.compareTime(task.startdate, settings.currentDate)? task.startdate: settings.currentDate, enddate : settings.compareTime(task.enddate, settings.currentDate)? task.enddate: settings.currentDate}
        $scope.profile = profile;
        $scope.equipments = equipments;
        $scope.equipment = equipments[0];
        $scope.currentURL = settings.currentURL;
        $scope.host = {"hostname":settings.hostname};
        $scope.currentDate = getCurrentDate();
        $scope.currentTime = getCurrentTime();
        $scope.code = task.code;
        $scope.release = task.release;
        $scope.settings = settings;
        $scope.equipmentTab = {startTimeForBorrowEquipment: (task.startdate > getCurrentDate()? task.startdate: getCurrentDate()) + "T00:00", endTimeForBorrowEquipment: task.enddate + "T12:00"};
        $scope.assignment = null;

        $scope.assignmentSubmit = function(){
        	this.assignment.taskid = task.id;
        	this.assignment.action = 'assignment';
        	$.post( settings.hostname + 'ajax.php', $scope.assignment, function( response){
        		response = JSON.parse( response.trim() );
        		$scope.task.notassignee = response.not_assignee;
        		$scope.task.assignee = response.assignee;
        		$scope.$apply();
	        	$( '#assignmentModal' ).hide();
	        	$( '.modal-backdrop' ).hide();
        	});
        }

        $scope.addAssignment = function( assignee ){
        	$scope.assignment = { staffid: assignee.id, name: assignee.name, duration: 0};
        }

        $scope.removeAssignment = function( assignmentId ){
        	var params = {action : 'removeAssignment', assignmentid : assignmentId}
        	$.post( settings.hostname + 'ajax.php', params, function( response){
        		response = JSON.parse( response.trim() );
        		$scope.task.notassignee = response.not_assignee;
        		$scope.task.assignee = response.assignee;
        		$scope.$apply();
        	});
        }

        $scope.selectEquipment = function(event){
            var id = event.target.attributes[1].value;
            if(id.split("/")[0] == "code")
            	if( id.split( '/' )[1] == 'release' ){
            		$scope.release = findElementInTree(task.release, id);
            	}else{
            		$scope.code = findElementInTree(task.code, id);
            	}
            else
                $scope.equipment = findElementInTree(equipments, id);
        }

        $scope.canFinish = function(){
            if( task.status != 1)
                return false;

            for( var i in task.children ){
            	if( task.children[i].status != 2){
            		return false;
            	}
            }

            return true;
        }
        $scope.canStart = function(){
            if( task.status == 0 && (task.parent == null || task.parent.status == 1))
                return true;

            return false;
        }
        $scope.canLogWork = function(){
            if(task.status != 1 )
                return false;

            return $.inArray(profile.id,task.assignee.map(function(e){return e.id})) != -1;
        }

        $scope.canCreateTask = function() {
        	return !(settings.compareTime(settings.currentDate, this.subtask.startdate) || settings.compareTime(this.subtask.startdate, task.startdate) || settings.compareTime(this.subtask.startdate, this.subtask.enddate) || settings.compareTime(this.subtask.enddate, task.enddate) );
        }
        $scope.isOwner = function(){
            return task.leaderid == profile.id;
        }

        $scope.isDone = function(){
        	return (task.status == 2);
        }
        $scope.isAssignee = function(){
        	if( this.isOwner() )
        		return false;
        	return $.inArray(profile.id,task.assignee.map(function(e){return e.id})) != -1;
        }
        $scope.isStart = function(){
        	return (task.status == 1);
        }
        $scope.isProject = function(){
        	return task.type == 'PROJECT';
        }

        $scope.canAccess = function(){
        	if( task.projecttype == 'PUBLIC' ){
        		return true;
        	}else{
        		return ( this.isAssignee() || this.isOwner() );
        	}
        }
        $scope.more = function( e ){
			$( e.target ).parent().children( '.children' ).slideToggle();
			if( $( e.target ).text() == 'Show' ){
				$( e.target ).text( 'Hide' );
			}else{
				$( e.target ).text( 'Show' );
			}
        }
    });

</script>
<div ng-app="myApp" ng-controller="projectController" ng-cloak>
    <div ng-show="canAccess()">
	    <h2><span class="label label-success">Project Management</span></h2>
	    <br>
        <a href="?cat=project">All project</a>
        <span ng-repeat="path in task.path"> ---> <a href="?cat=task&id={{path.id}}">{{path.name}}</a></span>
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>
                <li role="presentation" ng-if="isOwner() || profile.isLeader()"><a href="#subtask" aria-controls="subtask" role="tab" data-toggle="tab">Subtask</a></li>
                <li role="presentation" ng-if="isOwner() || isAssignee() || profile.isLeader()"><a href="#equipment" aria-controls="equipment" role="tab" data-toggle="tab">Equipment</a></li>
                <li role="presentation" ng-if="isOwner() || isAssignee()"><a href="#code" aria-controls="code" role="tab" data-toggle="tab">Code</a></li>
                <li role="presentation" ng-if="isOwner() || profile.isLeader()"><a href="#release" aria-controls="release" role="tab" data-toggle="tab">Release</a></li>
                <li role="presentation" ng-if="isOwner() || profile.isLeader()"><a href="#finish" aria-controls="finish" role="tab" data-toggle="tab">Report</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php includeAllPhp("function/task/tab/big")?>
            </div>
        </div>
	    <?php includeAllPhp("function/task/modal")?>
    </div>
    <div ng-show="!canAccess()">The page doesn't exist or Permission denied!</div>
</div>

<?php
        }else{
            showPermissionOrNotExists();
        }
    }else{
        showPermissionOrNotExists();
    }

?>

