<!-- Finished task -->
<div role="tabpanel" class="tab-pane" id="finish">
	<table>
		<tr>
			<th>Finish</th>
			<td style="width:70%">{{task.result}}</td>
		</tr>

		<tr>
			<th>All task in this {{task.type == 'PROJECT'? 'project': 'task'}}</th>
			<td>
				<span>{{task.report.tasks.length}}</span>
				<span ng-if="task.report.tasks.length != 0">
					(<span ng-repeat="(key, subtask) in task.report.tasks">{{key>0? ', ': ''}}<a href="?cat=task&id={{subtask.id}}">{{subtask.name}}</a></span>)
				</span>
			</td>
		</tr>

		<tr>
			<th>Tasks start early</th>
			<td>
				<span>{{task.report.startEarlyTask.length}}</span>
				<span ng-if="task.report.startEarlyTask.length != 0">
					(<span ng-repeat="(key, subtask) in task.report.startEarlyTask">{{key>0? ', ': ''}}<a href="?cat=task&id={{subtask.id}}">{{subtask.name}}</a></span>)
				</span>
			</td>
		</tr>

		<tr>
			<th>Task start late</th>
			<td>
				<span>{{task.report.startLateTask.length}}</span>
				<span ng-if="task.report.startLateTask.length != 0">
					(<span ng-repeat="(key, subtask) in task.report.startLateTask">{{key>0? ', ': ''}}<a href="?cat=task&id={{subtask.id}}">{{subtask.name}}</a></span>)
				</span>
			</td>
		</tr>

		<tr>
			<th>Task end early</th>
			<td>
				<span>{{task.report.endEarlyTask.length}}</span>
				<span ng-if="task.report.endEarlyTask.length != 0">
					(<span ng-repeat="(key, subtask) in task.report.endEarlyTask">{{key>0? ', ': ''}}<a href="?cat=task&id={{subtask.id}}">{{subtask.name}}</a></span>)
				</span>
			</td>
		</tr>

		<tr>
			<th>Task end late</th>
			<td>
				<span>{{task.report.endLateTask.length}}</span>
				<span ng-if="task.report.endLateTask.length != 0">
					(<span ng-repeat="(key, subtask) in task.report.endLateTask">{{key>0? ', ': ''}}<a href="?cat=task&id={{subtask.id}}">{{subtask.name}}</a></span>)
				</span>
			</td>
		</tr>

		<tr>
			<th>Assignee</th>
			<td>
				<div>{{task.report.assignee.length}}
					<span ng-if="!isDone() && task.report.assignee.length != 0">
						(<span ng-repeat="(key, assignee) in task.report.assignee">{{key>0? ', ': ''}}<a  href="?cat=staff&action=profile&id={{assignee.staff.id}}">{{assignee.staff.name}}</a></span>)
					</span>
				</div>

				<div class="more" ng-click="more( $event )" ng-if="task.report.assignee.length != 0 && isDone()">
					<div class="control">Show</div>
					<div class="children">
						<table>
							<tr>
								<th>Name</th>
								<th>Comment</th>
								<th>Rate</th>
							</tr>
							<tr ng-repeat="finish in task.finished">
								<td><a href="?cat=staff&action=profile&id={{finish.staff.id}}">{{finish.staff.name}}</a></td>
								<td ng-bind-html="finish.comment | html"></td>
								<td>{{finish.rate}}</td>
							</tr>
						</table>
					</div>
				</div>

			</td>
		</tr>
	</table>
</div>
