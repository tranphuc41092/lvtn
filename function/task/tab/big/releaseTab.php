            <div role="tabpanel" class="tab-pane" id="release">
                <div class="row">
                    <div class="col-md-3 index" >
                        <script type="text/javascript">
                            document.write('<ul id="releaseCategoryIndex" class="nav nav-stacked" role="tablist">')
                                generateCategoryIndex(task.release);
                            document.write("</ul>");

                            clickCategoryIndexEvent('#releaseCategoryIndex');
                        </script>
                    </div>
                    <div class="col-md-9 index">
                        <table ng-if="isOwner() && release.type == 'FILE'" >
                            <tr>
                                <td>
                                    <button class="btn btn-default" data-toggle="modal" data-target="#resetReleaseModal" title="Push code"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>Reset</button>
                                </td>
                            </tr>
                        </table>

                        <div>
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <td>{{release.name}}</td>
                                </tr>
                                <tr>
                                    <th>Create time</th>
                                    <td>{{release.filectime}}</td>
                                </tr>
                                <tr>
                                    <th>Size</th>
                                    <td>{{release.filesize}} bytes</td>
                                </tr>
<!--                                 <tr>
                                    <th>Folders</th>
                                    <td>
                                        <div ng-repeat="child in release.children">
                                            <span ng-if="child.type!='FILE'">{{child.name}}</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Files</th>
                                    <td>
                                        <div ng-repeat="child in release.children">
                                            <span ng-if="child.type=='FILE'">{{child.name}}</span>
                                        </div>
                                    </td>
                                </tr>
 -->

                            </table>
                        </div>
                    </div>
                </div>

            </div>
