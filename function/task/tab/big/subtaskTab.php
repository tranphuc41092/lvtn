<div role="tabpanel" class="tab-pane" id="subtask">
	<table ng-if="!isDone() && isOwner()" >
		<tr>
			<td>
				<button class="btn btn-default" data-toggle="modal" data-target="#newModal" title="New Task">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>NewTask
				</button>
			</td>
		</tr>
	</table>

    <table class="table table-striped table-bordered">
        <tr>
            <th>Name</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Duration</th>
            <th>Status</th>
        </tr>
        <tr ng-repeat="subtask in task.children">
            <td><a href="?cat=task&id={{subtask.id}}">{{subtask.name}}</a></td>
            <td>{{settings.convertNormalDate(subtask.startdate)}}</td>
            <td>{{settings.convertNormalDate(subtask.enddate)}}</td>
            <td>{{settings.convertNormalDuration(subtask.duration, true)}}</td>
            <td ng-switch="subtask.status">
                <span ng-switch-when="0">Not started</span>
                <span ng-switch-when="1">Proccessing</span>
                <span ng-switch-when="2">Completed</span>
            </td>
        </tr>
    </table>
</div>
