            <div role="tabpanel" class="tab-pane" id="equipment">
            	<table ng-if="!isDone() && isOwner()" >
            		<tr>
            			<td><button class="btn btn-default" data-toggle="modal" data-target="#addEquipmentModal" title="Add Equipment"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Equipment</button></td>
            		</tr>
	            </table>
				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>From</th>
							<th>To</th>
						</tr>
					</thead>
					<tr ng-repeat="equipment in task.equipment">
						<td><a href="?cat=equipment&action=detail&id={{equipment.id}}" target="_blank" title="View {{equipment.name}}'s Infomation ">{{equipment.name}}</td>
						<td>{{equipment.description}}</td>
						<td ng-if="equipment.assign!=null">{{settings.convertNormalDate(equipment.assign[equipment.assign.length-1].starttime)}}</td>
						<td ng-if="equipment.assign!=null">{{settings.convertNormalDate(equipment.assign[equipment.assign.length-1].endtime)}}</td>
					</tr>
				</table>

			</div>