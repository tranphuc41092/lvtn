                <div role="tabpanel" class="tab-pane active" id="detail">
                    <div class="main">
                        <div class="row">
                            <!-- Main area -->
                            <div class="col-md-8">
                            	<table ng-if="!isDone() && isOwner()" style="margin-bottom: 0.5cm" class="navbar navbar-default">
                            		<tr>
                            			<td>
			                                <form class="form" method="post" action="{{currentURL}}" ng-switch="task.status"  style="display: inline-block">
			                                    <input type="hidden" name="id" value="{{task.id}}"/>
			                                    <button type="submit" class="btn btn-success" name="action" value="doneTask" ng-switch-when="0" ng-if="!isStart()">Start</button>
			                                    <button type="submit" class="btn btn-success" name="action" value="doneTask" ng-switch-when="1" ng-if="!task.isProject() && canFinish()" >Done</button>
			                                </form>
			                                <span ng-if="isOwner()">
			                                	<button class="btn btn-danger" data-toggle="modal" data-target="#resultProject" ng-if="task.isProject() && canFinish()" ng-disabled="!canFinish()">Done</button>
			                                    <button class="btn btn-default" data-toggle="modal" data-target="#editModal" title="Edit Task"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>Edit</button>
			                                    <button class="btn btn-default" data-toggle="modal" data-target="#removeModal" title="Remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Remove</button>
			                                    <button class="btn btn-default" data-toggle="modal" data-target="#assignModal" title="Assign"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Assign</button>
			                                    <button class="btn btn-default" data-toggle="modal" data-target="#unassignModal" title="Unassign"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Unassign</button>
			                                    <button ng-if="isProject()" class="btn btn-default" data-toggle="modal" data-target="#releaseCodeModal" title="Release"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>Release</button>
			                                    <!-- <button ng-if="isProject() && task.hasTemporyCode" class="btn btn-default" data-toggle="modal" data-target="#loadCurrentCodeModal" title="Rename Code"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>Load unrelease</button> -->
			                                </span>
                            			</td>
                            		</tr>
	                            </table>

	                            <table ng-if="isStart() && isAssignee()"><tr><td>
	                            	<button class="btn btn-default" data-toggle="modal" data-target="#logtimeModal"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Log time</button>
	                            </td></tr></table>


                                <table  style="margin-bottom: 5px;">
                                    <tr>
                                        <th>Name</th>
                                        <td>{{task.name}}</td>
                                    </tr>
                                    <tr ng-if="task.isPorject()">
                                        <th>Type</th>
                                        <td>{{task.projecttype}}</td>
                                    </tr>
                                    <tr>
                                        <th>Leader</th>
                                        <td><a href="?cat=staff&action=profile&id={{task.leader.id}}">{{task.leader.name}}</a></td>
                                    </tr>
                                    <tr>
                                        <th>Start date</th>
                                        <td>{{settings.convertNormalDate(task.startdate)}}</td>
                                    </tr>
                                    <tr>
                                        <th>End date</th>
                                        <td>{{settings.convertNormalDate(task.enddate)}}</td>
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        <td>{{task.description}}</td>
                                    </tr>
                                    <tr>
                                        <th>Duration</th>
                                        <td>
	                                        <div class="row">
	                                            <div class="col-md-3">Logged</div>
	                                            <div class="col-md-5">
													<div class="progress">
													  	<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{settings.showProgress( task.timelog_of_task.logged, task.duration )}}%">
														</div>
													</div>
	                                            </div>
	                                            <div class="col-md-4">{{settings.convertNormalDuration(task.timelog_of_task.logged, true)}}</div>
	                                        </div>
	                                        <div class="row">
	                                            <div class="col-md-3">Estimated</div>
	                                            <div class="col-md-5">
													<div class="progress">
													  	<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{settings.showProgress( task.duration, task.timelog_of_task.logged )}}%"></div>
													</div>
	                                            </div>
	                                            <div class="col-md-4">{{settings.convertNormalDuration(task.duration, true)}}</div>
	                                        </div>
                                        </td>
                                    </tr>
                                    <tr ng-switch="task.status">
                                        <th>Status</th>
                                        <td ng-switch-when="0">Not started</td>
                                        <td ng-switch-when="1">Proccessing</td>
                                        <td ng-switch-when="2">Completed</td>
                                    </tr>
                                </table>

                                <div role="tabpanel" style="margin-top:20px;">

                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#comment" aria-controls="comment" role="tab" data-toggle="tab">Comment</a></li>
                                        <li role="presentation"><a href="#timelog" aria-controls="timelog" role="tab" data-toggle="tab">Time Log</a></li>
                                        <li role="presentation"><a href="#worklog" aria-controls="worklog" role="tab" data-toggle="tab">Work Log</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <?php
                                            includeAllPhp("function/task/tab/small");
                                        ?>
                                    </div>

                                </div>

                            </div>

                            <!-- Right area -->
                            <div class="col-md-4 alert alert-info" style="  background-color: rgb(223, 237, 159);">

                                <!-- Assignee -->
                                 <div>
                                    <div class="alert alert-success"><strong>Assignee</strong></div>
                                    <div class="assignee-area">
	                                    <table>
	                                    	<tr>
	                                    		<th>Name</th>
	                                    		<th>Estimated</th>
	                                    		<th ng-if="isOwner() && !isDone()">Action</th>
	                                    	</tr>
	                                        <tr ng-repeat="assignee in task.assignee">
	                                            <td>
	                                                <a href="?cat=staff&action=profile&id={{assignee.id}}" title="View {{assignee.name}}'s Information" target="_blank">{{assignee.name}}</a>
	                                            </td>
	                                            <td>{{settings.convertNormalDuration(assignee.estimated, true)}}</td>
	                                            <td ng-if="isOwner() && !isDone()">
                                                    <button ng-if="profile.id != assignee.id" class="btn btn-default" ng-click="removeAssignment( assignee.assignmentid )"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
	                                            </td>
	                                        </tr>
	                                    </table>
	                                </div>
                                </div>

                                <!-- Log work -->
                                <hr>
                                <div class="worklog-area" ng-if="profile.isLeader() || isAssignee()">
                                    <div class="alert alert-success"><strong>Your time Log</strong></div>
                                    <div style="background: white">
                                        <div class="row">
                                            <div class="col-md-3">Logged</div>
                                            <div class="col-md-5">
												<div class="progress">
												  	<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{settings.showProgress( task.timelog_of_assignee.logged, task.timelog_of_assignee.estimated )}}%">
													</div>
												</div>
                                            </div>
                                            <div class="col-md-4">{{settings.convertNormalDuration(task.timelog_of_assignee.logged, true)}}</div>
                                        </div>
                                        <div class="row" ng-if="isAssignee()">
                                            <div class="col-md-3">Estimated</div>
                                            <div class="col-md-5">
												<div class="progress">
												  	<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{settings.showProgress( task.timelog_of_assignee.estimated, task.timelog_of_assignee.logged )}}%"></div>
												</div>
                                            </div>
                                            <div class="col-md-4">{{settings.convertNormalDuration(task.timelog_of_assignee.estimated, true)}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
