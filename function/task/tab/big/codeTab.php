            <div role="tabpanel" class="tab-pane" id="code">
                <div class="row">
                    <div class="col-md-3 index">
                        <script type="text/javascript">
                            document.write('<ul id="codeCategoryIndex" class="nav nav-stacked" role="tablist">')
                                generateCategoryIndex(task.code);
                            document.write("</ul>");

                            clickCategoryIndexEvent('#codeCategoryIndex');
                        </script>
                    </div>
                    <div class="col-md-9 index">
                        <table ng-if="!isDone() && (isOwner() || isAssignee() )" >
                            <tr>
                                <td>
                                    <button ng-if="code.type=='CATEGORY'" class="btn btn-default" data-toggle="modal" data-target="#pushCodeModal" title="Push code"><span class="glyphicon glyphicon-open" aria-hidden="true"></span>Upload</button>
                                    <button class="btn btn-default" data-toggle="modal" data-target="#removeCodeModal" title="Remove code"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>Remove</button>
                                    <button ng-if="code.id=='code/code_'+task.id && task.hasTemporyCode" class="btn btn-default" data-toggle="modal" data-target="#loadCurrentCodeModal" title="Rename Code"><span class="glyphicon glyphicon-import" aria-hidden="true"></span>Load unrelease code</button>
                                    <button ng-disabled="code.id=='code/code_'+task.id" class="btn btn-default" data-toggle="modal" data-target="#renameCodeModal" title="Rename Code"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span>Rename</button>

                                    <form method="post" ng-submit="submit()" action="{{currentURL}}" style="display:inline-block">
                                        <input type="hidden" name="path" value="{{code.id}}"/>

                                       <!--  <button ng-if="code.type=='CATEGORY'" class="btn btn-default" value="zipanddownload" name="action">Zip and download</button>
                                        <a ng-if="code.type!='CATEGORY'" class="btn btn-default" href="{{code.id}}" target="_blank">Download</a> -->

                                    </form>
                                </td>
                            </tr>
                        </table>

                        <div>
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <td>{{code.name}}</td>
                                </tr>
                                <tr>
                                    <th>Create time</th>
                                    <td>{{code.filectime}}</td>
                                </tr>

                                <tr>
                                    <th>Size</th>
                                    <td>{{code.filesize}} bytes</td>
                                </tr>
                                <tr ng-if="code.type!='CATEGORY'">
                                	<th>Preview</th>
                                	<!-- <td>{{code.content}}</td> -->
                                	<td>
                                		<div ng-bind-html="code.content | html" style="max-width: 750px; max-height: 500; overflow: auto;"></div>
                                	</td>
                                </tr>
<!--                                 <tr>
                                    <th>Folders</th>
                                    <td>
                                        <div ng-repeat="child in code.children">
                                            <span ng-if="child.type!='FILE'">{{child.name}}</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Files</th>
                                    <td>
                                        <div ng-repeat="child in code.children">
                                            <span ng-if="child.type=='FILE'">{{child.name}}</span>
                                        </div>
                                    </td>
                                </tr>
 -->

                            </table>
                        </div>
                    </div>
                </div>

            </div>
