<div role="tabpanel" class="tab-pane" id="worklog">
    <div class="alert alert-info">
        <div style="min-height: 2cm; background: white" class="index">
            <table class="table" style="background: white">
                <tr ng-repeat="worklog in task.worklogs">
                    <td>
                        <a href="?cat=profile&id={{worklog.staff.id}}">{{worklog.staff.name}}</a> - {{settings.convertNormalDateTime(worklog.createtime)}} - {{worklog.comment}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
