<div role="tabpanel" class="tab-pane active" id="comment">
    <div class="alert alert-info comment">
        <div style="min-height: 2cm; background-color: white;" class="index">
            <div ng-repeat="comment in task.comment">
                <div><strong>{{comment.owner.name}}</strong> added a comment - {{settings.convertNormalDateTime(comment.createtime)}}</div>
                <div style="margin-left:1cm">{{comment.content}}</div>
                <hr />
            </div>
        </div>

        <form method="post" action="{{currentURL}}" ng-if="isOwner() || isAssignee()">
            <textarea type="text" size="50" rows="5" name="content" class="form-control" placeholder="Enter your comment"></textarea>
            <input type="hidden" name="action"  value="newComment"/>
            <input type="submit" class="btn btn-success" value="Send" style="margin-top:11px; margin-left:675px;" />
        </form>
    </div>
</div>
