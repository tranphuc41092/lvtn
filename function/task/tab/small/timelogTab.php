<div role="tabpanel" class="tab-pane" id="timelog">
    <div class="alert alert-info">
        <div style="min-height: 2cm; background: white" class="index">
            <table class="table" style="background: white">
                <tr ng-repeat="timelogElement in task.timelog_of_task.timelogs">
                    <td>
                        <a href="?cat=profile&id={{timelogElement.staff.id}}">{{timelogElement.staff.name}}</a> logged time - {{settings.convertNormalDateTime(timelogElement.createtime)}}
                        <div class="row">
                            <div class="col-md-3">Time spend:</div>
                            <div class="col-md-9">
                                {{settings.convertNormalDuration(timelogElement.duration, true)}}<br>
                                {{timelogElement.comment}} <span ng-if="timelogElement.comment == ''">&lt;no comment&gt;</span>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
