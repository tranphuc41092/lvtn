<div class="modal fade" id="resultProject" tabindex="-1" role="dialog" aria-labelledby="resultProjectModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 60%">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Result Project</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
					<div class="row">
						<ul class="nav nav-stacked col-md-4 scroll-area" role="tablist">
							<h4><label class="label label-success">Project</label><hr></h4>
							<li role="presentation" class="active">
								<a href="#all-project-result" role="tab" data-toggle="tab">{{task.name}}</a>
							</li>
							<h4><label class="label label-success">Member</label><hr></h4>
							<li role="presentation" ng-repeat="assignee in task.assigneed" >
								<a href="#staff{{assignee.id}}" role="tab" data-toggle="tab">{{assignee.name}}</a>
							</li>
						</ul>
						<div class="tab-content col-md-8 scroll-area">
							<div role="tabpanel" class="tab-pane active" id="all-project-result" >
		                        <center><h2>Project: {{task.name}}</h2></center>
		                        <h3>Comment</h3>
								<textarea name="commentTaskResult" class="form-control" rows="10"></textarea>
							</div>
							<div role="tabpanel" class="tab-pane" ng-repeat="assignee in task.assigneed" id="staff{{assignee.id}}" >
		                        <center><h2>Member: {{assignee.name}}</h2></center>
		                        <h3>Rate</h3>
										<input type="number" name="rate[{{assignee.id}}]" class="form-control" min="0" max="10" step="0.1" value="0" style="width:3cm"/>
		                        <h3>Comment</h3>
								<textarea name="comment[{{assignee.id}}]" class="form-control" rows="10"></textarea>
							</div>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="resultProject" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>