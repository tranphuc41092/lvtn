<div class="modal fade" id="resetReleaseModal" tabindex="-1" role="dialog" aria-labelledby="releaseModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Reset project code</h4>
            </div>

                <form class="form-horizontal" method="post" name="editForm" id="eidtForm" ng-submit="submit()" action="{{currentURL}}">
                    <div class="modal-body">
                    	<h3>Do you want to reset with this release?</h3>
                        <input ng-hide="true" name="id"  ng-model="task.id" />
                        <input type="hidden" name="releasePath" class="form-control" value="{{release.id}}" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary"name="action" value="resetRelease">OK</button>
                    </div>
            </form>
        </div>
    </div>
</div>
