<div class="modal fade" id="logtimeModal" tabindex="-1" role="dialog" aria-labelledby="logtimeModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Log time</h4>
            </div>
            <form method="post" action="{{currentURL}}" class="form-horizontal">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="duration" class="col-sm-3 control-label">Log time ({{settings.system_time_format.range[settings.system_time_format.value - 1].symbol}})</label>
                        <div class="col-sm-2">
                            <input type="number" class="form-control" id="duration" name="duration" value="0" step="0.1" min="0" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comment" class="col-sm-3 control-label">Comment</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="5" id="comment" name="comment" placeholder="Enter comments"></textarea>
                        </div>
                    </div>

                    <input type="hidden" name="taskid" value="{{task.id}}"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="logtime" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
