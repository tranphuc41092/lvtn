<div class="modal fade" id="addEquipmentModal" tabindex="-1" role="dialog" aria-labelledby="addEquipmentModalLabel" aria-hidden="true">
    <div class="modal-dialog"  style="width: 80%;">
        <div class="modal-content" style="overflow-y: auto">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Equipment</h4>
            </div>
            <div class="modal-body">
				<div style="margin-left: 35%; margin-bottom: 1cm">
	            	<form class="form-horizontal" action="{{currentURL}}" method="post">
		            	<table>
		            		<tr>
		            			<th width="20%">Start time</th>
		            			<td width="40%"><input type="datetime-local" class="form-control" ng-model="equipmentTab.startTimeForBorrowEquipment"/></td>
		            			<td width="40%"><div class="error" ng-if="compareTime(currentTime,equipmentTab.startTimeForBorrowEquipment)">Should be greater than current time</div></td>
		            		</tr>
		            		<tr>
		            			<th>End time</th>
		            			<td><input type="datetime-local" class="form-control" ng-model="equipmentTab.endTimeForBorrowEquipment"/></td>
		            			<td><div class="error" ng-if="compareTime(equipmentTab.startTimeForBorrowEquipment, equipmentTab.endTimeForBorrowEquipment)">Should be greater than Start time</div></td>
		            		</tr>
		            	</table>
		            </form>
		        </div>
				<div class="row">
					<div class="col-md-4" style="height: 300px; overlow-y: auto">
		                <script type="text/javascript">
	                        document.write('<ul id="categoryIndex" class="nav nav-stacked" role="tablist">')
	                            generateCategoryIndex(equipments);
	                        document.write("</ul>")
	                        clickCategoryIndexEvent('#categoryIndex');
		                </script>
					</div>
					<div class="col-md-8" style="height: 300px; overlow-y: auto">
						<div ng-if="equipment==null"></div>
						<div ng-if="equipment!=null">
							<table ng-if="equipment.type!='CATEGORY' && isOwner() && !isDone() && compareTime(equipmentTab.endTimeForBorrowEquipment, equipmentTab.startTimeForBorrowEquipment) && compareTime(equipmentTab.startTimeForBorrowEquipment, currentTime) && (equipment.assign==null || compareTime(equipmentTab.startTimeForBorrowEquipment,equipment.assign[equipment.assign.length-1].endtime))">
								<tr>
									<td>
										<form method="post" ng-submit="submit()" action="{{currentURL}}">
											<button class="btn btn-default" name="action" value="assignEquipment"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Assign</button>
											<input type="hidden" name="starttime" value="{{equipmentTab.startTimeForBorrowEquipment}}"/>
											<input type="hidden" name="endtime" value="{{equipmentTab.endTimeForBorrowEquipment}}"/>
											<input type="hidden" name="taskid" value="{{task.id}}"/>
											<input type="hidden" name="forid" value="{{equipment.id}}"/>
											<input type="hidden" name="type" value="Equipment"/>
										</form>
									</td>
								</tr>
							</table>
							<table class="table">
								<tr>
									<th>Name</th>
									<td>{{equipment.name}}</td>
								</tr>
								<tr>
									<th>Description</th>
									<td>{{equipment.description}}</td>
								</tr>
								<tr>
									<th>Type</th>
									<td>{{equipment.type}}</td>
								</tr>
			                    <tr ng-if="equipment.type=='CATEGORY'">
			                        <th>Subcategory</th>
			                        <td>
			                            <div ng-repeat="child in equipment.children">
			                                <div ng-if="child.type=='CATEGORY'">
			                                    <a href="?cat=equipment&action=detail&id={{child.id}}">{{child.name}}({{child.children.length}})</a>
			                                </div>
			                            </div>
			                        </td>
			                    </tr>

			                    <tr>
			                        <th>List of equipment</th>
			                        <td>
			                        	<div style="max-height: 200px; overflow-y:auto">
				                        	<table class="table">
					                            <tr ng-repeat="child in equipment.children" ng-if="child.type!='CATEGORY'">
					                                    <td><a href="?cat=equipment&action=detail&id={{child.id}}">{{child.name}}</a>
					                                    <td>{{child.description}}
					                            </tr>
				                        	</table>
				                        </div>
			                        </td>
			                    </tr>

			                    <tr ng-if="equipment.type!='CATEGORY'">
			                    	<th>History</th>
			                        <td>
			                        	<div class="scrollable" style="max-height: 200px">
				                            <table class="table">
				                                <thead>
				                                    <tr>
				                                        <th>Task</th>
				                                        <th>Start time</th>
				                                        <th>End time</th>
				                                    </tr>
				                                </thead>
				                                <tr ng-repeat="assign in equipment.assign">
				                                    <th>{{assign.task.name}}</th>
				                                    <td>{{assign.starttime}}</td>
				                                    <td>{{assign.endtime}}</td>
				                                </tr>
				                            </table>
				                        </div>
			                        </td>
			                    </tr>
							</table>

						</div>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>
