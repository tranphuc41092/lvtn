<div class="modal fade" id="loadCurrentCodeModal" tabindex="-1" role="dialog" aria-labelledby="loadCurrentCodeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Load unrelease code</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
                    <h3>Do you want to load unrelease code?</h3>
                    <input ng-hide="true" id="id" name="id" value="{{task.id}}" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="loadUnreleaseCode" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
