<div class="modal fade" id="renameCodeModal" tabindex="-1" role="dialog" aria-labelledby="pushCodeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Rename</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">From</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="text" value="{{code.name}}" readonly/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">To</label>
                        <div class="col-sm-9">
                            <input type="hidden" name="id" value="{{code.id}}" />
                            <input class="form-control" type="text" name="name" value="" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="renameCode" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
