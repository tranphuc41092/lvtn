<div class="modal fade" id="pushCodeModal" tabindex="-1" role="dialog" aria-labelledby="pushCodeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload code</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3>The current folder: <span>{{code.id.substr(5)}}</span></h3>
                    <input type="hidden" name="fileNames" id="fileNames" value=""/>
                    <input type="hidden" name="lastModified" id="lastModified" value=""/>
                    <input type="hidden" name="taskid" value="{{task.id}}"/>
                    <input type="hidden" name="path" value="{{code.id}}" />
                    <input class="form-control" type="file" id="files" name="files[]" webkitdirectory directory multiple />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="pushCode" class="btn btn-primary">OK</button>
                </div>
            </form>
            <script>
                var inputFile = document.getElementById('files');
                var fileNames = document.getElementById('fileNames');
                var lastModified = document.getElementById('lastModified');

                inputFile.onchange = function(e) {
                    var files = e.target.files; // FileList
                    var fileNameArr = "";
                    var lastModifiedArr = "";

                    for (var i = 0, f; f = files[i]; ++i){
                        fileNameArr += files[i].webkitRelativePath + ",";
                        lastModifiedArr += files[i].lastModified + ",";
                    }

                    fileNames.value = fileNameArr;
                    lastModified.value = lastModifiedArr;
                }
            </script>
        </div>
    </div>
</div>
