<div class="modal fade" id="removeCodeModal" tabindex="-1" role="dialog" aria-labelledby="removeCodeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Remove {{code.type=='CATEGORY'? 'folder': 'file'}}</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
                    <h3>Do you really want to remove this {{code.type=='CATEGORY'? 'folder': 'file'}}?</h3>
                    <br>
                    <input class="form-control" type="text" value="{{code.id.substr(5)}}" readonly/>

                    <input ng-hide="true" id="id" name="id" value="{{code.id}}" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="removeCode" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
