        <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header alert alert-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">New Task</h4>
                    </div>

                    <form class="form-horizontal" method="post" name="newForm" id="newForm" ng-submit="submit()" action="{{currentURL}}">
                        <div class="modal-body" >
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" placeholder="Enter name" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="startdate" class="col-sm-3 control-label">Start date</label>
                                <div class="col-sm-4">
                                    <input id="startdate" name="startdate" type="date" class="form-control" ng-model="subtask.startdate">
                                </div>
                                <div class="col-md-5 error">
                                	<div ng-if="settings.compareTime(settings.currentDate, subtask.startdate)">Should be greater than current date</div>
                                	<div ng-if="settings.compareTime(subtask.startdate, task.startdate)">Must be start after {{task.type=='PROJECT'? 'project': 'task'}}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="enddate" class="col-sm-3 control-label">End date</label>
                                <div class="col-sm-4">
                                    <input id="enddate" name="enddate" type="date" class="form-control" ng-model="subtask.enddate">
                                </div>
                                <div class="col-sm-5 error">
                                	<div ng-if="settings.compareTime(subtask.startdate, subtask.enddate)">Should be greater than Start time</div>
                                	<div ng-if="settings.compareTime(subtask.enddate, task.enddate)">Must end before {{task.type=='PROJECT'? 'project': 'task'}}</div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="duration" class="col-sm-3 control-label">Duration ({{settings.getSystemTimeFormatSymbol()}})</label>
                                <div class="col-sm-9">
                                    <input id="duration" name="duration" type="text" class="form-control" style="width:40%" value="0">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <textarea type="text" name="description" class="form-control" rows="5" placeholder="Enter description"></textarea>
                                </div>
                            </div>

                            <input type="hidden" name="action" value="newTask" />
                            <input ng-hide="true" id="belongto" name="belongto"  ng-model="task.id" />
                        </div>
                        <div class="modal-footeng-disabledr">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary" ng-disabled="!canCreateTask()">OK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
