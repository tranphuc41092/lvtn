<div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Remove {{task.type=='PROJECT'? "project": "task"}}</h4>
            </div>

            <form class="form-horizontal" method="post" name="removeForm" id="removeForm" action="{{currentURL}}">
                <div class="modal-body">
                    <h3><span>Do you really want to remove this {{task.type=='PROJECT'? "project": "task"}}?</span></h3>

                    <input type="hidden" name="action" value="removeTask" />
                    <input ng-hide="true" id="id" name="id" ng-model="task.id" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
