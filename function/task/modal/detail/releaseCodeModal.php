<div class="modal fade" id="releaseCodeModal" tabindex="-1" role="dialog" aria-labelledby="releaseCodeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Release project</h4>
            </div>

            <form class="form-horizontal" method="post" action="{{currentURL}}" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id" class="col-sm-3 control-label">Release_</label>
                        <div class="col-sm-6">
                            <input type="text" name="releasename" class="form-control" value="" required autofocus>
                            <input type="hidden" name="id" class="form-control" value="{{task.id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="release" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
