<div class="modal fade" id="unassignModal" tabindex="-1" role="dialog" aria-labelledby="unassignModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Unassign Member in Task</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
                    <table class="table">
                        <tr ng-repeat="assignee in task.assignee">
                            <td>
                                <input type="checkbox" name="assignments[]" value="{{assignee.assignmentid}}">
                                <a href="?cat=staff&action=profile&id={{assignee.id}}" target="_blank" title="View {{assignee.name}}'s Infomation ">{{assignee.name}}</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" name="action" value="unassignTask" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
