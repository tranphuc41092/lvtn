<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="assignModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert alert-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Assign Member in Task</h4>
            </div>
            <form class="form-horizontal" method="post" action="{{currentURL}}">
                <div class="modal-body">
                    <table class="table">
                    	<tr>
                    		<th>Name</th>
                    		<th>Duration({{settings.getSystemTimeFormatSymbol()}})</th>
                    	</tr>
                        <tr ng-repeat="notassignee in task.notassignee">
                            <td>
                                <a href="?cat=staff&action=profile&id={{notassignee.id}}" target="_blank" title="View {{notassignee.name}}'s Infomation ">{{notassignee.name}}</a>
                            </td>
                            <td class="col-sm-4"><input type="number" class="form-control" name="durations[{{notassignee.id}}]" value="0" min="0" max="999999" step="0.1"/></td>
                        </tr>
                    </table>
                    <input type="hidden" name="taskid" value="{{task.id}}"/>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="submit" name="action" value="assignTask" class="btn btn-primary">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>

