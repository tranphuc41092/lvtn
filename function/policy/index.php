<script type="text/javascript">
    var app = angular.module('myApp', []);
    app.controller('settingsController', function($scope) {
    	$scope.settings = settings;
    	$scope.currentURL = document.URL;
    	$scope.profile = profile;
    });
</script>

<div ng-app="myApp" ng-controller="settingsController">
    <h2><span class="label label-success">Policy</span></h2>
    <br>

    <form class="form" method="post" ng-submit="submit()" action="{{currentURL}}">
    	<div class="col-md-6 col-md-offset-3">
	    	<table>
	    		<tr>
	    			<th>Attribute</th>
	    			<th>Value</th>
	    		</tr>
	    		<tr ng-repeat="setting in settings" ng-if="setting.type && setting.type!=-1">
	    			<td>{{setting.name}}</td>
	    			<td>
		                <select ng-if="setting.type==2" class="form-control" name="{{setting.id}}">
		                    <!-- Select -->
		                    <option  ng-repeat="range in setting.range" ng-selected="setting.value==range.id" value="{{range.id}}">{{range.symbol}}</option>
		                </select>
		    			<input ng-if="setting.type==1" type="number" class="form-control" name="{{setting.id}}" value="{{setting.value}}" min="{{setting.min}}" max="{{setting.max}}" />
		                <div ng-if="setting.type==0">{{setting.value}}</div>
	    			</td>
	    		</tr>
	    	</table>
	    	<button ng-if="profile.isAdmin()" type="submit" name="action" value="setting" class="btn btn-primary col-md-2 col-md-offset-4" style="margin-top: 1cm">Save</button>
	    </div>
    </form>

</div>