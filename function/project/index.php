<?php
    $task = new Task();
    $projects = $task->find(array("type"=>"PROJECT"));
    $owner = null;

    $projectSize = sizeof($projects);

    for($i = 0; $i < $projectSize; $i++){
        $projects[$i]->owner = $projects[$i]->getLeader();
    }

    $assignment = new Assignment();
    $assignments = $assignment->find( array( 'staffid' => $__profile->id ), array( 'staffid', 'taskid') );
    $assignedTasks = array();
    foreach ($assignments as $key => $assignment) {
    	$assignedTasks[] = new Task( $assignment->taskid );
    }

    $sql = "SELECT * FROM timelog WHERE staffid = ".$__profile->id." ORDER BY date";
    $rows = mysql_query($sql);
    $dates = array();
    $timelogs = array();
    while( $row = mysql_fetch_array( $rows ) ){
        $dates[$row['date']] = $row['date'];
        if( !isset( $timelogs[$row['taskid']]) ){
            $timelogs[$row['taskid']] = array();
            $timelogs[$row['taskid']]['task'] = new Task( $row['taskid'] );
            $timelogs[$row['taskid']]['dates'] = array();
        }

        if( !isset( $timelogs[$row['taskid']]['dates'][$row['date']] ) ){
            $timelogs[$row['taskid']]['dates'][$row['date']] = array();
        }
        $timelogs[$row['taskid']]['dates'][$row['date']][] = new Timelog( $row );
    }

    echo '
        <script>
            var projects = '.json_encode($projects, JSON_UNESCAPED_UNICODE).';
            var assignedTasks = '.json_encode($assignedTasks, JSON_UNESCAPED_UNICODE).';
            var timelogs = '.json_encode($timelogs, JSON_UNESCAPED_UNICODE).';
            var datelogs = '.json_encode($dates, JSON_UNESCAPED_UNICODE).';
        </script>';

?>
    <script type="text/javascript">
        var app = angular.module('myApp', []);
        app.controller('projectController', function($scope) {
            $scope.projects = projects;
            $scope.project = {startdate : settings.currentDate, enddate : settings.currentDate};
            $scope.profile = profile;
            $scope.currentURL = document.URL;
            $scope.settings = settings;
            $scope.host = {"hostname":getHostName()};
            $scope.assignedTasks = assignedTasks;
            $scope.timelogs = timelogs;
            $scope.datelogs = datelogs;
            $scope.timelog = {id:10, duration:0, comment:''};
            $scope.searchProject = function( $event){
                var key = event.target.value;
                $scope.projects = [];
                projects.forEach(function(e){
                    if( e.name.toLowerCase().indexOf(key.toLowerCase()) != -1)
                        $scope.projects.push(e);
                });
            }

            $scope.editTimelog = function( log ){
                $scope.timelog = log;
            }
        });
    </script>
<div ng-app="myApp" ng-controller="projectController">
    <h2><span class="label label-success">Project Management</span></h2>
    <br>

    <div role="tabpanel">
	    <!-- Nav tabs -->
	    <ul class="nav nav-tabs" role="tablist">
	        <li role="presentation" class="active"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Projects</a></li>
	        <li role="presentation" ng-if="profile.isLeader() || profile.isEngineer()"><a href="#workspace" aria-controls="workspace" role="tab" data-toggle="tab">Workspace</a></li>
	    </ul>
        <div class="tab-content">
            <?php includeAllPhp("function/project/tab/big")?>
        </div>
    </div>

    <?php includeAllPhp("function/project/modal"); ?>
</div>