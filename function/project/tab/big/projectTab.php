<div role="tabpanel" class="tab-pane active" id="project">
    <div class="form-inline">
        <button ng-if="profile.isLeader()" class="btn btn-default" data-toggle="modal" data-target="#newModal" title="New Project"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>New Project</button>
        <input type="text" class="form-control" ng-model="keyword" placeholder="Project name" ng-keyup="searchProject( $event )">
    </div>

    <hr>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Type</th>
                <th>Start date</th>
                <th>End date</th>
                <th>Leader</th>
                <th>Status</th>
            </tr>
        </thead>
        <tr ng-repeat="project in projects">
            <td><a href="?cat=task&id={{project.id}}">{{project.name}}</a></td>
            <td>{{project.projecttype == 'PRIVATE'? "Private": "Public"}}</td>
            <td>{{settings.convertNormalDate(project.startdate)}}</td>
            <td>{{settings.convertNormalDate(project.enddate)}}</td>
            <td><a href="?cat=staff&action=profile&id={{project.owner.id}}">{{project.owner.name}}</td>
            <td ng-switch="project.status">
                <div ng-switch-when="0">Not started</div>
                <div ng-switch-when="1">Proccessing</div>
                <div ng-switch-when="2">Completed</div>
            </td>
        </tr>
    </table>
</div>