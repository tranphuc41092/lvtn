<div role="tabpanel" class="tab-pane" id="workspace">

	<div role="tabpanel" style="margin-top: 0.5cm">
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#notstarted" aria-controls="notstarted" role="tab" data-toggle="tab">Not started</a></li>
			<li role="presentation"><a href="#processing" aria-controls="processing" role="tab" data-toggle="tab">Processing</a></li>
            <li role="presentation"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab">Completed</a></li>
            <li role="presentation"><a href="#timelogs" aria-controls="completed" role="tab" data-toggle="tab">Time logs</a></li>
		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="notstarted">
				<table>
					<tr>
						<th>Name</th>
		                <th>Start date</th>
		                <th>End date</th>
						<th>Duration</th>
						<th>Project/task?</th>
					</tr>
					<tr ng-repeat="task in assignedTasks" ng-if="task.status == 0">
						<td><a href="?cat=task&id={{task.id}}">{{task.name}}</a></td>
			            <td>{{settings.convertNormalDate(task.startdate)}}</td>
			            <td>{{settings.convertNormalDate(task.enddate)}}</td>
						<td>{{settings.convertNormalDuration(task.duration)}} day</td>
						<td>{{task.type=='PROJECT'? 'Project': 'Task'}}</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="processing">
				<table>
					<tr>
						<th>Name</th>
		                <th>Start date</th>
		                <th>End date</th>
						<th>Duration</th>
						<th>Project/task?</th>
					</tr>
					<tr ng-repeat="task in assignedTasks" ng-if="task.status == 1">
						<td><a href="?cat=task&id={{task.id}}">{{task.name}}</a></td>
			            <td>{{settings.convertNormalDate(task.startdate)}}</td>
			            <td>{{settings.convertNormalDate(task.enddate)}}</td>
						<td>{{settings.convertNormalDuration(task.duration)}} day</td>
						<td>{{task.type=='PROJECT'? 'Project': 'Task'}}</td>
					</tr>
				</table>
			</div>
			<div role="tabpanel" class="tab-pane" id="completed">
				<table>
					<tr>
						<th>Name</th>
		                <th>Start date</th>
		                <th>End date</th>
						<th>Duration</th>
						<th>Project/task?</th>
					</tr>
					<tr ng-repeat="task in assignedTasks" ng-if="task.status == 2">
						<td><a href="?cat=task&id={{task.id}}">{{task.name}}</a></td>
			            <td>{{settings.convertNormalDate(task.startdate)}}</td>
			            <td>{{settings.convertNormalDate(task.enddate)}}</td>
						<td>{{settings.convertNormalDuration(task.duration)}} day</td>
						<td>{{task.type=='PROJECT'? 'Project': 'Task'}}</td>
					</tr>
				</table>
			</div>

            <div role="tabpanel" class="tab-pane" id="timelogs">
                <div style="width: 100%; overflow: auto">
                    <table>
                        <tr>
                            <th>Unit ({{settings.getSystemTimeFormatSymbol()}})</th>
                            <th ng-repeat="date in datelogs">{{date}}</th>
                        </tr>
                        <tr ng-repeat="log in timelogs">
                            <th style="min-width: 5cm"><a href="?cat=task&id={{log.task.id}}">{{log.task.name}}</a></th>
                            <td ng-repeat="date in datelogs">
                                <div ng-if="log.dates[date] != null" style="min-width: 4cm">
                                    <div ng-repeat="(key, timelog) in log.dates[date]">
                                        <button ng-click="editTimelog(timelog)" class="btn btn-default" data-toggle="modal" data-target="#editTimelogModal" title="Edit Task"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>
                                        {{settings.convertNormalDuration(timelog.duration, true)}}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
		</div>
	</div>
</div>