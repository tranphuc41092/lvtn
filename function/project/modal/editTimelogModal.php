    <!--New Modal -->
    <div class="modal fade" id="editTimelogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header alert alert-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit timelog</h4>
                </div>
                <form class="form-horizontal" method="post" name="newForm" id="newForm" ng-submit="submit()" action="{{currentURL}}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="duration" class="col-sm-3 control-label">Duration({{settings.getSystemTimeFormatSymbol()}})</label>
                            <div class="col-sm-9">
                                <input id="duration" name="duration" type="number" min="0" step="0.1" class="form-control" style="width:40%" value="{{settings.convertNormalDuration(timelog.duration)}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="comment" class="col-sm-3 control-label">Comment</label>
                            <div class="col-sm-9">
                                <textarea type="text" name="comment" class="form-control" rows="5" placeholder="Enter description">{{timelog.comment}}</textarea>
                            </div>
                        </div>
                        <input ng-hide="true" name="id"  value="{{timelog.id}}" />
                        <input ng-hide="true" name="date"  value="{{timelog.date}}" />
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" name="action" value="editTimelog">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
