    <!--New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header alert alert-success">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New project</h4>
                </div>
                <form class="form-horizontal" method="post" name="newForm" id="newForm" ng-submit="submit()" action="{{currentURL}}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control" placeholder="Enter name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Project type</label>
                            <div class="col-sm-3">
                                <select name="projecttype" class="form-control">
                                	<option value="PUBLIC">Public</option>
                                	<option value="PRIVATE">Private</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="comment" class="col-sm-3 control-label">Start date</label>
                            <div class="col-sm-4">
                                <input type="date" name="startdate" class="form-control" ng-model="project.startdate" />
                            </div>
                            <div class="col-md-5 error" ng-if="settings.compareTime(settings.currentDate, project.startdate)">Should be greater than current date</div>
                        </div>

                        <div class="form-group">
                            <label for="comment" class="col-sm-3 control-label">End date</label>
                            <div class="col-sm-4">
                        		<input type="date" name="enddate" class="form-control" ng-model="project.enddate" />
                            </div>
                            <div class="col-sm-5 error" ng-if="settings.compareTime(project.startdate, project.enddate)">Should be greater than Start time</div>
                        </div>

                        <div class="form-group">
                            <label for="duration" class="col-sm-3 control-label">Duration ({{settings.getSystemTimeFormatSymbol()}})</label>
                            <div class="col-sm-9">
                                <input id="duration" name="duration" type="text" class="form-control" style="width:40%" value="0">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="comment" class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-9">
                                <textarea type="text" name="description" class="form-control" rows="5" placeholder="Enter description"></textarea>
                            </div>
                        </div>

                        <input ng-hide="true" id="belongto" name="belongto"  value="0" />
                        <input type="hidden" name="leaderid" value="{{profile.id}}" />
                        <input type="hidden" name="action" value="newProject" />
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" ng-disabled="settings.compareTime(settings.currentDate, project.startdate) || settings.compareTime(project.startdate, project.enddate)">OK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
