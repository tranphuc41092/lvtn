<?php
	include "core.php";
	includeAllPhp("utils");
	includeAllPhp("model");
	function convertDuration(){
		$__settings = new Settings();
		$__settings = convert_assoc($__settings->find(), "variable");
		$__settings["system_time_format"]->range = $__settings["system_time_format"]->getChildren();
	    $convert = 1;
	    for( $i =  $__settings['system_time_format']->value - 1; $i < sizeof( $__settings['system_time_format']->range ); $i++ ){
	        $convert *= $__settings['system_time_format']->range[$i]->value;
	    }
	    return $convert;
	}
	authenticate();
	$worklog = new Worklog();
	$worklog->staffid=$__profile->id;
	switch ( $_POST['action']) {
		case 'assignment':
			$assignment = new Assignment( $_POST );
			$assignment->duration = floatval($_POST['duration']) * convertDuration();
			$assignment->save();
			$task = new Task( $_POST['taskid'] );
			$notAssignee = json_encode($task->getNotAssignee(), JSON_UNESCAPED_UNICODE);
			$assignee = json_encode($task->getAssignee(), JSON_UNESCAPED_UNICODE);
			echo '{"not_assignee":'.$notAssignee.', "assignee":'.$assignee.'}';
			break;

		case 'removeAssignment':
			$assignment = new Assignment( $_POST['assignmentid'] );
			$assignment->remove();
			$task = new Task( $assignment->taskid );
			$notAssignee = json_encode($task->getNotAssignee(), JSON_UNESCAPED_UNICODE);
			$assignee = json_encode($task->getAssignee(), JSON_UNESCAPED_UNICODE);
			echo '{"not_assignee":'.$notAssignee.', "assignee":'.$assignee.'}';

			$worklog->taskid = $assignment->taskid;
			$worklog->comment = 'unassigned some persons!';
			$worklog->save();

			break;

		default:
			echo '';
			break;
	}

?>