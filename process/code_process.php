<?php
	// var_dump($_POST);
	if(isset($_POST["action"])){
		switch($_POST["action"]){
			case 'pushCode':
				$task = new Task($_POST["taskid"]);
				$project = $task->getProject();
				$fileNames = explode(",", $_POST["fileNames"]);
				$code = new Code($_POST["path"]);
				$fileSize = count($_FILES["files"]["name"]);
				$lastModified = explode(",", $_POST["lastModified"]);

				for($i = 0; $i < $fileSize; $i++){
					$code->moveUpload($_FILES["files"]["tmp_name"][$i], $fileNames[$i], $lastModified[$i]);
				}
				reloadCurrentPage();
				break;
			case 'zipanddownload':
				$code = new Code($_POST["path"]);
				$code->zip();
				// zip($code->id, "code/zip/".$code->name);
				break;
			case 'removeCode':
				$code = new Code($_POST["id"]);
				$code->remove();
				reloadCurrentPage();
				break;
			case 'renameCode':
				$code = new Code($_POST["id"]);
				$code->rename($_POST["name"]);
				reloadCurrentPage();
				break;
		}

	}

?>

