<?php
function convertDuration(){
    $__settings = new Settings();
    $__settings = convert_assoc($__settings->find(), "variable");
    $__settings["system_time_format"]->range = $__settings["system_time_format"]->getChildren();
    $convert = 1;
    for( $i =  $__settings['system_time_format']->value - 1; $i < sizeof( $__settings['system_time_format']->range ); $i++ ){
        $convert *= $__settings['system_time_format']->range[$i]->value;
    }
    return $convert;
}

if(!authenticate()){
	if(isset($_POST["action"])){
		global $__post_result;
		switch($_POST["action"]){
			case "login":
				$staff = new Staff();
				$profiles = $staff->find(Array("username"=>$_POST["username"]));
				$__post_result['object'] = $_POST;
				if(sizeof($profiles) > 0){
					if($profiles[0]->password == $_POST["password"]){
						if(isset($_POST["remember"]))
							setcookie("id", $profiles[0]->id, time() + 365*24*3600);
						else
							setcookie("id", $profiles[0]->id);
					}else{
						setcookie("id", "", time() - 1);
						break;
					}
				}else{
					setcookie("id", "", time() - 1);
					break;
				}
				reloadCurrentPage();
				break;
			case "register":
				$profile = new Staff($_POST);
				$__post_result["object"] = $_POST;

				if($profile->username == "" || $profile->password == "" || $profile->name == "" || $profile->repassword == ""){
					$__post_result["text"] = "Not enought information!";
					break;
				}

				if(strlen($profile->username) < 8){
					$__post_result["text"] = "User name length must be greater than 7";
					break;
				}

				if(strlen($profile->password) < 8){
					$__post_result["text"] = "Password length must be greater than 7";
					break;
				}

				if($profile->password != $profile->repassword){
					$__post_result["text"] = "Password is not match!";
					break;
				}

				$otherProfile = $profile->find(array("username"=>$profile->username));
				if(sizeof($otherProfile) > 0){
					$__post_result["text"] = "User name is exist!";
					break;
				}

				$profile->save();
				setcookie( "id", $profile->getNextID() - 1 );
				reloadCurrentPage();
				break;
		}
	}
}else{
	if(isset($_POST["action"])){
		switch ($_POST["action"]) {
			case "logout":
				setcookie("id", "", time() - 1);
				reloadCurrentPage();
				break;
			case "editStaff":
				if(authenticate()->position=="HUMAN"){
					$staff = new Staff();
					$profiles = $staff->find(array("id", $_POST["id"]));
					if(sizeof($profiles) > 0){
						$profile = $profiles[0];
						$profile->name = $_POST["name"];
						$profile->sex = $_POST["sex"];
						$profile->birthdate = $_POST["birthdate"];
						$profile->address = $_POST["address"];
						$profile->telephone = $_POST["telephone"];
						$profile->email = $_POST["email"];
						$profile->position = $_POST["position"];
						$profile->description = $_POST["description"];
						$profile->save();
					}
				}else{
					alert("You are not permission!");
				}
				break;
		}
	}
}

?>
