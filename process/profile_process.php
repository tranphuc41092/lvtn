<?php
if(isset($_POST["action"])){
	switch ($_POST["action"]) {
		case "editProfile":
			if(authenticate()->id == $_POST["id"]){
				$profile = authenticate();
				$profile->avatar = changeAvatar( "images/avatar/profile", $profile->avatar );

				$profile->name = $_POST["name"];
				$profile->password = $_POST["password"];
				$profile->repassword = $_POST["repassword"];
				$profile->sex = $_POST["sex"];
				$profile->birthdate = $_POST["birthdate"];
				$profile->address = $_POST["address"];
				$profile->telephone = $_POST["telephone"];
				$profile->email = $_POST["email"];
				$profile->description = $_POST["description"];
				$profile->save();
				reloadCurrentPage();
			}else{
				alert("You are not permission!");
			}
			break;
		case 'createStaff':
			$staff = new Staff($_POST);

			$staff->save();
			break;
		case 'removeStaff':
			$staff = new Staff();
			$condition = array("id"=>$_POST["id"]);
			$staffs = $staff->find($condition);
			if(sizeof($staffs) > 0){
				$staffs[0]->remove();
			}
			break;
		case 'editStaff':
			$staff = new Staff($_POST);
			$staff->save();
			break;
        case 'editTimelog':
            $timelog = new Timelog( $_POST['id'] );
            $timelog->duration = floatval( $_POST['duration'] ) * convertDuration();
            $timelog->comment = $_POST['comment'];
            $timelog->date = $_POST['date'];
            $timelog->save();
            break;
	}
}

?>