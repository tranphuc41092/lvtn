<?php
	if(isset($_POST["action"])){
		switch ($_POST["action"]) {
			case 'removeMember':
				$staff = new Staff($_POST["id"]);
				$staff->teamid = "0";
				$staff->save();
				break;
			case 'newTeam':
				$team = new Team($_POST);
				$team->avatar = changeAvatar( "images/avatar/team", $team->avatar );
				if($team->name == ""){
					setPostResult("Team name is empty");
					break;
				}
				$team->save();
				$teamId = $team->getNextID() - 1;

				if( isset( $_POST["members"] ) ){
					foreach ($_POST["members"] as $member) {
						$staff = new Staff($member);
						$staff->teamid = $teamId;
						$staff->save();
					}
				}
				$staff = new Staff($team->leaderid);
				$staff->teamid = $teamId;
				$staff->position = 'LEADER';
				$staff->save();

				break;
			case 'editTeam':
				$team = new Team($_POST["id"]);

				$leader = new Staff($team->leaderid);
				$leader->teamid = 0;
				$leader->position = 'STAFF';
				$leader->save();

				$team->avatar = changeAvatar( "images/avatar/team", $team->avatar );
				$team->name = $_POST["name"];
				$team->description = $_POST["description"];
				$team->leaderid = $_POST["leaderid"];
				$team->save();

				$leader = new Staff( $_POST["leaderid"] );
				$leader->teamid = $team->id;
				$leader->position = 'LEADER';
				$leader->save();

				$members = $team->getMember();
				foreach ($members as $member) {
					$member->teamid = "0";
					$member->save();
				}

				if(isset($_POST["members"])){
					foreach ($_POST["members"] as $memberId) {
						$member = new Staff($memberId);
						$member->teamid = $team->id;
						$member->save();
					}
				}
				break;
		}
	}
?>