<?php
	if(isset($_POST["action"])){
		switch ($_POST["action"]) {
			case 'newEquipment':
				$equipment = new Equipment($_POST);
				$equipment->avatar = changeAvatar( "images/avatar/equipment", $equipment->avatar );
				$equipment->save();
				reloadCurrentPage();
				break;
			case 'removeEquipment':
				$equipment = new Equipment();
				$condition = array("id"=>$_POST["id"]);
				$equipments = $equipment->find($condition);
				if(sizeof($equipments) > 0){
					$equipments[0]->remove(true);
				}
				reloadCurrentPage();
				break;
			case 'editEquipment':
				$equipment = new Equipment($_POST["id"]);
				$equipment->set("name", $_POST["name"]);
				$equipment->set("description", $_POST["description"]);
				$equipment->avatar = changeAvatar( "images/avatar/equipment", $equipment->avatar );
				$equipment->save();
				reloadCurrentPage();
				break;
		}
	}

?>