<?php
	if(isset($_POST["action"])){
		$__profile = authenticate();
		switch($_POST["action"]){
			case 'newComment':
				$class = ucfirst($_GET["cat"]);
				$commentAttrArray = $_POST;
				$commentAttrArray["class"] = $class;
				$commentAttrArray["forid"] = $_GET["id"];
				$commentAttrArray["staffid"] = $__profile->id;

				$comment = new Comment($commentAttrArray);
				$comment->save();
				reloadCurrentPage();
				break;
			case 'editComment':
				reloadCurrentPage();
				break;
			case 'removeComment':
				reloadCurrentPage();
				break;
		}
	}

?>