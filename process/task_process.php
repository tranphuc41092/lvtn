<?php
	if(isset($_POST["action"])){
		$ratio = convertDuration();
		$profile = authenticate();

		$worklog = new Worklog();
		$worklog->staffid=$__profile->id;

		switch ($_POST["action"]) {
			case 'newTask':
				$task = new Task($_POST);
				$task->leaderid = $__profile->id;
				$task->duration = convertDuration()* floatval( $_POST['duration'] );

				$id = $task->getNextID();
				$task->save();

				$assignee = new Assign();
				$assignee->taskid = $id;
				$assignee->forid = $task->getProject()->leaderid;
				$assignee->type = "Staff";
				$assignee->save();

				$worklog->taskid = $id;
				$worklog->comment = 'Create new task!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'removeTask':
				$task = new Task( $_POST["id"] );

				if( $task->id ){
					$task->remove(true);
				}

				$worklog->taskid = $task->id;
				$worklog->comment = 'Remove task!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'editTask':
				$task = new Task( $_POST["id"] );

				if( $task->id ){
					$task->set("name", $_POST["name"]);
					$task->set("description", $_POST["description"]);
					$task->set("startdate", $_POST["startdate"]);
					$task->set("enddate", $_POST["enddate"]);
					$task->set("duration", convertDuration() * floatval($_POST["duration"]));
					if( isset( $_POST['projecttype']) ){
						$task->projecttype = $_POST['projecttype'];
					}
					$task->save();

					$worklog->taskid = $task->id;
					$worklog->comment = 'Eddited task!';
					$worklog->save();
				}
				// reloadCurrentPage();
				break;
			case 'doneTask':
				$task = new Task($_POST["id"]);
				$task->status = floatval($task->status) + 1;
				if( $task->status == 1 ){
					$worklog->comment = 'started this task!';
					$task->start = (new DateTime())->format( 'Y-m-d' );
				}else{
					$task->end = (new DateTime())->format( 'Y-m-d' );
					$worklog->comment = 'closed this task!';
				}
				$task->save();

				$worklog->taskid = $task->id;
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'assignTask':
				$assignee = new Assignment();
				$assignee->taskid = $_POST["taskid"];
				$assigneeNumber = 0;
				foreach ($_POST["durations"] as $staffid => $duration) {
					if( $duration != 0){
						$assignee->staffid = $staffid;
						$assignee->duration = floatval( $duration ) * $ratio;
						$assignee->save();
						$assigneeNumber++;
					}
				}

				$worklog->taskid = $task->id;
				$worklog->comment = 'assigned  '.$assigneeNumber.' engineer(s) to task!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'unassignTask':
				$assigment_ids = $_POST['assignments'];
				if( !is_array( $_POST["assignments"] ) ){
					$assigment_ids = array( $_POST['assignments'] );
				}

				foreach ($assigment_ids as $assigment_id) {
					$assignment = new Assignment( $assigment_id );
					$assignment->endtime = date( 'Y-m-d h:m:s' );
					$assignment->save();
					$assignment->remove();
				}
				$worklog->taskid = $task->id;
				$worklog->comment = 'unassigned  some engineer(s)!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'assignEquipment':
				$assign = new Assign($_POST);
				$assign->save();

				$worklog->taskid = $_POST['taskid'];
				$worklog->comment = 'added equipment to task!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'logtime':
				$logtime = new Timelog($_POST);
				$logtime->duration = floatval($_POST["duration"]) * convertDuration();
				$logtime->staffid = $__profile->id;
				$logtime->save();

				$worklog->taskid = $_POST['taskid'];
				$worklog->comment = 'logged time!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'newProject':
				$task = new Task($_POST);

				$task->set("id", "");
				$task->set("type", "PROJECT");
				$task->set("belongto", "0");
				$task->duration = floatval( $_POST['duration'] ) * convertDuration();

				$id = $task->getNextID();
				$task->save();

				$assignee = new Assign();
				$assignee->taskid = $id;
				$assignee->forid = $task->leaderid;
				$assignee->type = "Staff";
				$assignee->save();

				$assignment = new Assignment();
				$assignment->staffid = $task->leaderid;
				$assignment->taskid = $id;
				$assignment->save();

				$worklog->taskid = $id;
				$worklog->comment = 'Create new project!';
				$worklog->save();

				reloadCurrentPage();
				break;
			case 'removeProject':
				$task = new Task();
				$condition = array("id"=>$_POST["id"], "type" => "PROJECT");
				$tasks = $task->find($condition);
				if(sizeof($tasks) > 0){
					$tasks[0]->remove(true);
				}
				$worklog->taskid = $_POST["id"];
				$worklog->comment = 'Remove project!';
				$worklog->save();

				reloadCurrentPage();
				break;
	         case 'resultProject':
	            $taskid = $_GET['id'];
	            $task_result = new Task_result();
	            $task_result->taskid = $taskid;
	            $comments = $_POST['comment'];
	            foreach ($comments as $staffid => $comment) {
	               $task_result->staffid = $staffid;
	               $task_result->comment = $comment;
	               $task_result->rate = $_POST['rate'][$staffid];
	               $task_result->save();
	            }
	            $task = new Task( $taskid );
	            $task->status = 2;
	            $task->result = $_POST['commentTaskResult'];
	            $task->end = (new DateTime())->format( 'Y-m-d' );
	            $task->save();
	            reloadCurrentPage();
	            break;
	        case 'release':
	        	$code = new Code( 'code/code_'.$_POST['id']);
	        	$code->zipTo( 'code/release/code_'.$_POST['id'].'/release_'.$_POST['releasename'] );

				$worklog->taskid = $_POST["id"];
				$worklog->comment = 'released the new version: release_'.$_POST['releasename'];
				$worklog->save();

	        	reloadCurrentPage();
	        	break;
	       	case 'resetRelease':
	        	$code = new Code( 'code/code_'.$_POST['id']);
	        	$code->zipTo( 'code/tmp/code_'.$_POST['id'] );
	        	$code->remove();
	        	$code = new Code( $_POST['releasePath'] );
	        	$code->unzipTo( 'code/code_'.$_POST['id'] );

				$worklog->taskid = $_POST["id"];
				$worklog->comment = 'reset release!';
				$worklog->save();

	       		reloadCurrentPage();
	       		break;
	       	case 'loadUnreleaseCode':
	       		$code = new Code( 'code/code_'.$_POST['id'] );
	       		$code->remove();

	       		$code = new Code( 'code/tmp/code_'.$_POST['id'].'.zip' );
	       		$code->unzipTo( 'code/code_'.$_POST['id']);
	       		$code->remove();

				$worklog->taskid = $_POST["id"];
				$worklog->comment = 'loaded drap code!';
				$worklog->save();

	       		reloadCurrentPage();
	       		break;
		}
	}

?>
