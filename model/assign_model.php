<?php
	class Assign extends Core{
		public $id = null;
		public $taskid = "";
		public $forid = "";
		public $starttime = "";
		public $endtime = "";
		public $type = "";

		public function unassign($taskid, $type, $forid = "0"){
			$assigns = $this->find(array("taskid"=>$taskid, "type"=>$type), false);
			foreach ($assigns as $assign) {
				$assign->remove();
			}
		}

		public function find($object = null, $is_get_type = true){
			$assigns = parent::find($object);
			if( $is_get_type ){
				$objectArray = array();
				foreach ($assigns as $assign) {	
					$assign->task = new Task($assign->taskid);
					if(isset($objectArray[$assign->forid])){
						array_push($objectArray[$assign->forid]->assign, $assign);
					}else{
						$className = $assign->type;
						$object = new $className($assign->forid);					
						$object->assign = array($assign);
						$objectArray[$assign->forid] = $object;
					}

				}

				$realObjectArray = array();

				foreach ($objectArray as $object) {
					array_push($realObjectArray, $object);
				}
				return $realObjectArray;
			}else{
				return $assigns;
			}
		}
	}

?>