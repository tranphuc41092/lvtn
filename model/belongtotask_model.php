<?php
class BelongToTask extends Core{
		public function find($object = null, $isDistinct = null){

			$typeObjects = parent::find($object, $isDistinct);
			$assign = new Assign();

			foreach ($typeObjects as $typeObject) {
				if(isset($this->belongto)){
					$typeObject->getStructureChildren();
				}
				// $typeTempObjects = $assign->find(array("type"=>$this->getClassName(), "forid"=>$typeObject->id));

			}
			return $typeObjects;
		}

		public function getStructureChildren(){
			$task = new Task();
			$assign = new Assign();

			$assigns = $assign->find(array("type"=>$this->getClassName(), "forid"=>$this->id));
			if(sizeof($assigns) == 0){
				$this->assign = null;
			}else{
				$this->assign = $assigns[0]->assign;
			}

			$children = parent::getChildren();
			foreach ($children as $child) {
				$child->getStructureChildren();
			}
			$this->children = $children;
		}
}

?>