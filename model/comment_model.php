<?php
	class Comment extends Core{
		public $id = null;
		public $lastmodified = "";
		public $class  = "";
		public $forid = "";
		public $staffid = "";
		public $content = "";

		public function find($args = null, $isDistinct = null){
			$comments = parent::find($args, $isDistinct);
			for($i = 0; $i < sizeof($comments); $i++){
				$comment = $comments[$i];
				$comment->owner = $this->getOwner($comment->staffid);
			}
			return $comments;
		}

	}

?>