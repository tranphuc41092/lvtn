<?php
	class Code{
		public $id = null;
		public $name = "";
		public $children = null;
		public $filemtime = "";
		public $filectime = "";
		public $type = "CATEGORY";
		public $content = "";

		public function __construct($id = null){
			if($id){
				if($id[strlen($id) - 1] == "/")
					$id = substr($id, 0, strlen($id) - 1);

				$this->id = $id;
				$pathArray = explode("/", $id);
				$this->name = $pathArray[sizeof($pathArray) - 1];
				if(!file_exists($this->id)){
					$this->mkdirectory($this->id);
				}
				$this->filemtime = date("d F Y H:i:s.", filemtime($this->id) );
				$this->filectime = date("d F Y H:i:s.", filectime($this->id) );
				$this->filesize = filesize($this->id);
				$this->getChildrenDirectory();
			}
		}

		public function remove(){
			if($this->type == 'CATEGORY')
				return $this->delTree($this->id);
			else
				return unlink($this->id);
		}

		public function rename( $name ){
			rename($this->id, $this->getParent()->id."/".$name);
			$this->id = $this->getParent()->id."/".$name;
			$this->name = $name;
		}

		private function delTree($dir) {
			$files = array_diff(scandir($dir), array('.','..'));
			foreach ($files as $file) {
				(is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
    		}
		    return rmdir($dir);
		}

  		public function mkdirectory($path){
			$directories = explode("/", $path);
			$path = "";

			foreach ($directories as $directory) {
				if($directory != ""){
					$path = $path."/".$directory;
					if(!file_exists(substr($path,1))){
						mkdir(substr($path,1));
					}
				}
			}
		}

		public function moveUpload($file, $path, $lastModified){
			$path = $this->id."/".$path;
			$pathArray = explode("/", $path);

			$fileName = $pathArray[count($pathArray) - 1];
			$pathArray = array_diff($pathArray, array($fileName));
			$directory = implode("/", $pathArray);

			if(!is_dir($path))
				$this->mkdirectory($directory);
			move_uploaded_file($file, $path);

			touch($path, $lastModified);
		}

		public function copyTo( $dest ){
			$source = $this->id;
			mkdir('./'.$dest, 0755);
			foreach (
			 $iterator = new \RecursiveIteratorIterator(
			  new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
			  \RecursiveIteratorIterator::SELF_FIRST) as $item
			) {
			  if ($item->isDir()) {
			    mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
			  } else {
			    copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
			  }
			}
		}

		private function getChildrenDirectory(){
			$path = $this->id;
			if(is_dir($path)){
				$children = array();
		    	foreach (glob("{$path}/*") as $filename)
		    	{
		    		$child = new Code($filename);
		    		$this->filesize += $child->filesize;
	    			array_push($children, new Code($filename));
		    	}
		    	$this->children = $children;
			}else{
				$this->type = "FILE";
				$imageType = array( 'jpg', 'png');
				$textType = array( 'txt', 'java', 'asp', 'php', 'html', 'js', 'css', 'cpp', 'cs', 'jsp', 'rb' );
				$extension = pathinfo ($path, PATHINFO_EXTENSION );

				if( exif_imagetype( $path ) != false ) {
					list( $width, $height ) = getimagesize( $path );
					if( $width > 700 ){
						$this->content = '<img src="'.$path.'" width="700px" />';
					}else{
						$this->content = '<img src="'.$path.'" width="'.$width.'px" />';
					}
				}else{
					if( in_array( $extension, $textType ) ){
						$this->content = str_replace( '<br />', '</code><br /><code>', highlight_file( $path, true ) );
					}else{
						$this->content = '<span class="error">Cannot preview this file!</span>';
					}
				}
			}
		}

		public function zipTo( $path = ''){
			$source = $this->id;
			$destination = ($path == ''? $this->name.".zip": $path.'.zip');

			// Get real path for our folder
			$rootPath = realpath('./'.$source);

			// Initialize archive object
			$zip = new ZipArchive();
			$zip->open($destination, ZipArchive::CREATE | ZipArchive::OVERWRITE);
			// Create recursive directory iterator
			/** @var SplFileInfo[] $files */
			$files = new RecursiveIteratorIterator(
			    new RecursiveDirectoryIterator($rootPath),
			    RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file)
			{
			    // Skip directories (they would be added automatically)
			    if (!$file->isDir())
			    {
			        // Get real and relative path for current file
			        $filePath = $file->getRealPath();
			        $relativePath = substr($filePath, strlen($rootPath) + 1);
			        // Add current file to archive
			        // $zip->addFile($filePath, $relativePath);
					$content = file_get_contents( $filePath );
					$zip->addFromString( $relativePath, $content);
			    }
			}

			// Zip archive will be created only after closing object
			$zip->close();
		}

		function unzipTo($destination){
			$source = $this->id;
			$zip = new ZipArchive;
			$res = $zip->open($source);
			if ($res === TRUE) {
			  $zip->extractTo($destination);
			  $zip->close();
			}
		}

		function getParent(){
			return new Code(substr($this->id, 0, strrpos($this->id, "/".$this->name)));
		}
	}
?>