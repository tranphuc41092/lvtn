<?php
	class Core{
		public $createtime;
		public $active = "1";

		public function __construct($object = null){
			if($object){
				if(is_array($object)){
					$varList = get_class_vars($this->getTableName());
					foreach ($varList as $var => $value) {
						if(isset($object[$var]))
							$this->set($var, $object[$var]);
					}
				}elseif(is_object($object)){
					$varList = get_class_vars($this->getTableName());
					foreach ($varList as $var => $value) {
						$this->set($var, $object->$var);
					}
				}else{
					$id = $object;
					$objs = mysql_query("SELECT * FROM ".$this->getTableName()." WHERE active = '1' AND id = '$id'");
					while ($object = mysql_fetch_array($objs)) {
						$varList = get_class_vars($this->getTableName());
						foreach ($varList as $var => $value) {
							if(isset($object[$var]))
								$this->set($var, $object[$var]);
						}
					}
				}

			}
			$this->createtime = date("Y-m-d H:i:s");
		}

		public function getPath(){
			if($this->belongto == "0")
				return array(array("id"=>$this->id, "name"=>$this->name));

			$parent = $this->getParent();

			return array_merge($parent->getPath(),array(array("id"=>$this->id, "name"=>$this->name)));
		}

		public function save(){
			if($this->id){
				$varList = get_class_vars($this->getClassName());
				$sqlQuery = array();
				foreach ($varList as $var => $value) {
					$sqlQuery[] = $var." = '".$this->get($var)."'";
				}
				mysql_query("UPDATE ".$this->getTableName()." SET ".implode( ", ", $sqlQuery)." WHERE id = '".$this->id."'");
			}else{
				$this->id = "";

				$varList = get_class_vars($this->getClassName());
				$sqlQuery1 = "";
				$sqlQuery2 = "";

				$flag = false;
				foreach ($varList as $var => $value) {
					if(!$flag){
						$sqlQuery1.=$var;
						$sqlQuery2.="'".$this->get($var)."'";
					}else{
						$sqlQuery1.=",".$var;
						$sqlQuery2.=",'".$this->get($var)."'";
					}
					$flag = true;
				}
				mysql_query("INSERT INTO ".$this->getTableName()."(".$sqlQuery1.") VALUES(".$sqlQuery2.")");
			}
		}

		public function remove($isRecursive = false){
			mysql_query("UPDATE ".$this->getTableName()." SET active = 0 WHERE id = '".$this->id."'");
			if($isRecursive && in_array("belongto", array_keys(get_class_vars(get_called_class())))){
				$children = $this->getChildren();
				foreach ($children as $key => $child) {
					$child->remove($isRecursive);
				}
			}
		}

		public function getChildren($isRecursive = false){
			$objArray = array();
			$tableName = $this->getTableName();
			$className = $this->getClassName();

			$objs = mysql_query("SELECT * FROM ".$tableName." WHERE active = '1' AND belongto = '".$this->get("id")."'");

			while($obj = mysql_fetch_array($objs)){
				$child = new $className($obj);
				array_push($objArray, $child);
				if($isRecursive)
					$objArray = array_merge($objArray, $child->getChildren($isRecursive));
			}

			return $objArray;
		}

		public function getParent(){
			$tableName = $this->getTableName();
			$className = $this->getClassName();

			$objs = mysql_query("SELECT * FROM ".$tableName." WHERE active = '1' AND id = '".$this->belongto."'");

			while($obj = mysql_fetch_array($objs)){
				return new $className($obj);
			}

			return null;
		}

		public function find($args =  null, $isDistinct = null){
			$objArray = array();
			$className = get_called_class();
			$tableName = strtolower($className);

			// $sql = " WHERE active='1'";
			$sql = array();

			if($args){
				foreach ($args as $key => $value) {
               if( $key != 'active' )
   					$sql[] = $key."='".$args[$key]."'";
				}
				if( !isset( $args['active'] ) ){
					$sql[] = "active = '1'";
				}else{
               if( $args['active'] == 1){
                  $sql[] = "active = '1'";
               }
            }
			}else{
				$sql[] = "active = '1'";
			}
			$objs = mysql_query( "SELECT ".($isDistinct? "DISTINCT ".implode(", ", $isDistinct): "*")." FROM ".$tableName.(sizeof($sql) > 0?" WHERE ".implode( " AND ", $sql ): "") );
			while($obj = mysql_fetch_array($objs)){
				array_push($objArray, new $className($obj));
			}
			return $objArray;
		}

		public function getComment($isForId = true){
			$commentObj = new Comment();
			if($isForId)
				$comments = $commentObj->find(array("class"=>$this->getClassName(), "forid"=>$this->id));
			else
				$comments = $commentObj->find(array("class"=>$this->getClassName()));

			return $comments;
		}

		public function getOwner($staffid){
			$staff = new Staff($staffid);
			if($staff->id)
				return $staff;

			return null;
		}


		public function newComment($commentData){
			$comment = new Comment($commentData);
			$comment->forid = $this->id;
			$comment->class = $this->classname;

			$comment->save();
		}

		public function set($variable, $value){
			$this->$variable = $value;
		}

		public function get($variable){
			return $this->$variable;
		}

		public function getNextID(){
			return mysql_fetch_array(mysql_query("SELECT `auto_increment` as id FROM INFORMATION_SCHEMA.TABLES WHERE table_name = '".strtolower(get_class($this))."'"))["id"];
		}

		public function getTableName(){
			return strtolower(get_called_class());
		}

		public function getClassName(){
			return get_called_class();
		}
	}
?>