<?php
	class Staff extends BelongToTask{
		public $id = null;
		public $address = "";
		public $email = "";
		public $name = "";
		public $password = "";
		public $repassword = "";
		public $telephone = "";
		public $username = "";
		public $sex = "";
		public $teamid = "0";
		public $birthdate = "";
		public $position = "STAFF";
		public $description = "";
		public $avatar = "";

		public function getExperience(){
			$task_result = new Task_result();
			$results = $task_result->find( array( 'staffid' => $this->id ) );
			foreach ($results as $key => $result ) {
				$results[$key]->project = new Task( $result->taskid );
			}
			return $results;
		}

		public function isStaff(){
			return ($this->position == 'STAFF');
		}

		public function isLeader(){
			return ($this->position == 'LEADER' );
		}

		public function isAdmin(){
			return ($this->position == 'ADMIN' );
		}
	}
?>