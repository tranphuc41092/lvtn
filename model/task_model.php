<?php
	class Task extends Core{
		public $id = null;
		public $name = "";
		public $description = "";
		public $leaderid = "";
		public $startdate = "";
		public $enddate = "";
		public $type = "TASK";
		public $belongto = 0;
		public $duration = "0";
		public $status = 0;
		public $result = '';
		public $start = '';
		public $end = '';
		public $projecttype = 'PUBLIC';

		public function isProject(){
			return ($this->type == "PROJECT");
		}

		public function getLeader(){
			return $this->getOwner($this->leaderid);
		}

		public function getNotAssignee(){
			$leader = new Staff( $this->leaderid );
			$team = new Team( $leader->teamid );
			$members = $team->getMember();

			$assignees = $this->getAssignee();
			$notAssignees = array();

			$staffIDs = array();
			foreach($assignees as $staff){
				array_push($staffIDs, $staff->id);
			}

			foreach($members as $member){
				if(!in_array($member->id, $staffIDs) )
					$notAssignees[] = $member;
			}

			return $notAssignees;
		}

		public function getAssignee(){
         $assignment = new Assignment();
         $assignments = $assignment->find( array("taskid"=>$this->id ) );
         $assignees = array();
         foreach( $assignments as $assignment ){
         	$staff = new Staff( $assignment->staffid );
         	$staff->assignmentid = $assignment->id;
         	$staff->estimated = $assignment->duration;
         	$assignees[] = $staff;
         }
         return $assignees;
		}

      	public function getAssigneed(){
        	$assignment = new Assignment();
        	$assignments = $assignment->find(array("taskid"=>$this->id, "active" => "0"), array('staffid', 'taskid'));
        	foreach( $assignments as $key => $assignment ){
        		$assignments[$key]->staff = new Staff( $assignment->staffid );
        	}
        	return $assignments;
      	}

      	public function get_worklog(){
      		$worklog = new Worklog();
      		return $worklog->find( array( 'taskid' => $this->id ) );
      	}

	    public function getFinishedResult(){
    	    $task_result = new Task_result();
        	$results = $task_result->find( array( 'taskid' => $this->id ) );
         	foreach ($results as $key => $result) {
         		$results[$key]->staff = new Staff( $result->staffid );
        	}
        	return $results;
      	}

		public function getTimelog_of_task(){
			$timelog = new Timelog();
			$timelogs = $timelog->find(array("taskid"=>$this->id));
			$duration = 0;
			foreach( $timelogs as $key=>$timelog){
				$duration += intval( $timelog->duration);
				$timelogs[$key]->staff = new Staff( $timelog->staffid );
			}
			return array( 'logged' => $duration, 'timelogs' =>$timelogs);
		}

		public function getTimelog_of_assignee( $staffid ){
			$timelog = new Timelog();
			$timelogs = $timelog->find( array( 'taskid'=> $this->id, 'staffid'=>$staffid) );
			$duration = 0;
			foreach( $timelogs as $key=>$timelog){
				$duration += intval( $timelog->duration);
				$timelogs[$key]->staff = new Staff( $timelog->staffid );
			}

			$assignment = new Assignment();
			$assignments = $assignment->find( array('taskid'=>$this->id, 'staffid'=>$staffid), array( 'staffid', 'taskid', 'duration') );
			$estimated = 0;
			if( sizeof( $assignments ) != 0){
				$estimated = $assignments[sizeof($assignments) - 1]->duration;
			}

			return array( 'logged' => $duration, 'timelogs' =>$timelogs, 'estimated' => $estimated);
		}

		public function getProject(){
			if($this->type == 'PROJECT')
				return new Task( $this->id );
			else
				return $this->getParent()->getProject();

			return null;
		}

		public function getReport(){
			if( $this->isProject() ){
				$report = array();
				$allTask = $this->getChildren( true );
				$report['tasks'] = $allTask;
				$report['assignee'] = $this->getAssigneed();


				$startLateTask = array();
				$endLateTask = array();
				$startEarlyTask = array();
				$endEarlyTask = array();

				foreach ($allTask as $task) {
					if( $task->start > $task->startdate ){
						$startEarlyTask[] = $task;
					}

					if( $task->start < $task->startdate ){
						$startLateTask[] = $task;
					}

					if( $task->end > $task->enddate ){
						$endEarlyTask[] = $task;
					}

					if( $task->end < $task->enddate ){
						$endLateTask[] = $task;
					}
				}

				$report['startLateTask'] = $startLateTask;
				$report['endLateTask'] = $endLateTask;
				$report['startEarlyTask'] = $startEarlyTask;
				$report['endEarlyTask'] = $endEarlyTask;


				return $report;
			}else{
				return null;
			}
		}
		public function remove($isRecursive = false){
			parent::remove($isRecursive);
			$assign = new Assign();
			$assign->unassign($this->id, "Staff");
		}

		public function hasTemporyCode(){
			return file_exists( 'code/tmp/code_'.$this->getProject()->id.'.zip' );
		}
	}
?>