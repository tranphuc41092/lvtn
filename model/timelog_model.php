<?php
	class Timelog extends Core{
		public $id = null;
		public $staffid = "";
		public $taskid = "";
		public $comment = "";
		public $duration = 0;
        public $date = '';

        public function __construct( $object = null ){
            parent::__construct( $object );
            $this->date = $this->date == ''? date( 'Y-m-d' ): $this->date;
        }
		public function unassign($taskid){
			$assigns = $this->find(array("taskid"=>$taskid));
			foreach ($assigns as $assign) {
				$assign->remove();
			}
		}

		public function find($args = null, $isDistinct = null){
			$worklogs = parent::find($args, $isDistinct);
			for($i = 0; $i < sizeof($worklogs); $i++){
				$worklog = $worklogs[$i];
				$worklog->owner = $this->getOwner($worklog->staffid);
			}
			return $worklogs;
		}
	}
?>