<?php
	class Worklog extends Core{
		public $id = null;
		public $staffid = "";
		public $comment = "";
		public $taskid = "";

		public function find($args = null, $isDistinct = null){
			$worklogs = parent::find($args, $isDistinct);
			foreach( $worklogs as $key => $worklog ){
				$worklogs[$key]->staff = new Staff( $worklog->staffid );
			}
			return $worklogs;
		}
	}
?>