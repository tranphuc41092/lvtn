<?php
	class Team extends Core{
		public $id = null;
		public $name = "";
		public $leaderid = 0;
		public $belongto = 0;
		public $description = '';
		public $avatar = "";

		public function __construct( $object = null ){
			parent::__construct( $object );
			if( $this->avatar == "" ){
				$this->avatar = "images/avatar/team/teamwork.png";
			}
		}

		public function getLeader(){
			$leader = new Staff($this->leaderid);
			return $leader;
		}

		public function getMember(){
			$staff = new Staff();
			return $staff->find(array("teamid"=>$this->id, "position" => 'STAFF' ) );
		}

		public function find($object = null, $isDistinct = null){
			$teams = parent::find($object, $isDistinct);
			foreach ($teams as $team) {
				$team->leader = $team->getLeader();
				$team->members  = $team->getMember();
			}

			return $teams;
		}
	}
?>