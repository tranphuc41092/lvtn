<?php
	define("SITE", getCurrentHostURL());
	define("HOST", getCurrentHostURL());
	define("CAT", "cat");
	define("ACTION", "action");

	function authenticate(){
		if(isset($_COOKIE["id"])){
			global $__profile;
			$staff = new Staff();
			$profiles = $staff->find(array("id"=>$_COOKIE["id"]));
			if(sizeof($profiles) > 0){
				$__profile = $profiles[0];
				return $profiles[0];
			}
		}
		$__profile = null;
		return null;
	}

	function getPostResult(){
		if(isset($_COOKIE["postresult"])){
			$postresult = $_COOKIE["postresult"];
			setcookie("postresult", "", time() - 1);
			return $postresult;
		}

		return null;
	}

	function setPostResult($value = ""){
		setcookie("postresult", $value, time() + 365*24*3600);
	}

	function includeAllPhp($folder){
    	foreach (glob("{$folder}/*") as $filename)
    	{
    		if(is_dir($filename)){
    			includeAllPhp($filename);
    		}else
		        include $filename;
    	}
	}

	function getAllFiles( $folder ){
		$files = array();
    	foreach (glob("{$folder}/*") as $filename)
    	{
    		if(is_dir($filename)){
    			$files = array_merge( $files, getAllFiles($filename) );
    		}else{
		        $files[] = file_get_contents( $filename );
    		}
    	}
    	return $files;
	}

	function convert_assoc( $array, $key ){
		$new_array = array();
		foreach ($array as $element) {
			$new_array[$element->$key] = $element;
		}

		return $new_array;
	}

	function alert($string){
		echo "<script>alert('".$string."')</script>";
	}

	function getCurrentHostURL(){
		$currentPageURL = getCurrentPageURL();
		return pathinfo( $currentPageURL, PATHINFO_DIRNAME ) == 'http://localhost'? $currentPageURL."/": pathinfo( $currentPageURL, PATHINFO_DIRNAME )."/";
	}

    function getCurrentPageURL() {
		 $pageURL = 'http';
		 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
			$pageURL .= "://";
		 if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		 } else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		 }
		 return $pageURL;
    }

    function reloadCurrentPage(){
    	header("Location: ".getCurrentPageURL());
    }

    function home(){
    	echo '
    	<script>
    		alert("You logged in!")
    		window.location.href="'.getCurrentHostURL().'";
    	</script>';
    }

	function reload($link = null){
		if($link){
			return "Location: ".getCurrentHostURL().$link;
		}else{
			return "Location: ".getCurrentPageURL();
		}
	}

	function changeAvatar( $default_directory, $old_avatar ){
		global $_FILES;
		define ("MAX_SIZE","100");
		function getExtension($str) {
			$i = strrpos($str,".");
			if (!$i) { return ""; }
			$l = strlen($str) - $i;
			$ext = substr($str,$i+1,$l);
			return $ext;
		}

		$errors=0;
		$image=$_FILES['avatar']['name'];
		if ($image){
			$filename = stripslashes($_FILES['avatar']['name']);
			$extension = getExtension($filename);
			$extension = strtolower($extension);
			if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")){
				echo 'Sorry, only JPG, JPEG, PNG & GIF files are allowed.!';
				$errors=1;
			}

			else{
				$image_name=time().'.'.$extension;
				$newname=$default_directory."/".$image_name;
				if( move_uploaded_file($_FILES['avatar']['tmp_name'], $newname)){
					if($old_avatar != "")
						unlink($old_avatar);

					return $newname;
				}
			}
		}
		return "";//error
	}

	function showPermissionOrNotExists(){
		echo 'The page doesn\'t exist or Permission denied!';
	}

?>